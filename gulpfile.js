var gulp = require("gulp"),
	//minifycss = require('gulp-minify-css'),
	rename = require("gulp-rename"),
	sass = require("gulp-sass"),
	livereload = require("gulp-livereload"),
	connect = require("gulp-connect"),
	notify = require("gulp-notify"),
	uglify = require("gulp-uglify"),
	concat = require("gulp-concat"),
	sourcemaps = require("gulp-sourcemaps"),
	browserSync = require("browser-sync").create();
var reload = browserSync.reload;

var paths = {
	css:["./skin/**/css/*.css"],
	js:["./js/*.js"],
	jssize:["./libs/app.js"],
	html:["./*.html"]
};


gulp.task("browser-sync", function() {
	browserSync.init({
		server: {
			baseDir: "./",
			
		},
		port:8081
	});
});


gulp.task("css", function() {
	return gulp.src("./css/*.css")
		.pipe(connect.reload());
});

gulp.task("html", function() {
	return gulp.src(paths.html)
		// .pipe(browserSync.stream())
		.pipe(reload({stream: true}));
	//.pipe(connect.reload());
});

gulp.task("js", function() {
	return gulp.src(paths.js)
		.pipe(connect.reload());
});

gulp.task("jsmin", function() {
	return gulp.src("./libs/app.js")
		.pipe(rename({ suffix: ".min" }))
		.pipe(uglify())
		.pipe(gulp.dest("./libs/min"));
});

gulp.task("jsconcat", function() {
	return gulp.src(["./js/scene.js"])
	    .pipe(concat("main.js"))
		.pipe(rename({ suffix: ".min" }))
		//.pipe(uglify())
		.pipe(gulp.dest("./libs/min"));
});

gulp.task("sass", function() {
	return gulp.src("./sass/style.scss")
		.pipe(sourcemaps.init({loadMaps: true}))
		.pipe(sass().on("error",sass.logError))
		.pipe(sourcemaps.write("maps"))
		.pipe(gulp.dest("./css"))
		.pipe(browserSync.stream());
});


gulp.task("watch",["sass"],function() {
	gulp.watch("./sass/**/*.scss", ["sass"]);
	gulp.watch(paths.css, ["css"]);
	gulp.watch(paths.js, ["js"]);
	gulp.watch("./js/*.js",["jsconcat"]);
	gulp.watch(paths.html, ["html"]);
	gulp.watch(paths.css).on("change", browserSync.reload);
});

gulp.task("default",["browser-sync","watch"]);
