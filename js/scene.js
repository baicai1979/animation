document.addEventListener("touchmove", function(e) {
		
	var evt = e || window.event;
	if(evt.preventDefault) {
		evt.preventDefault();
	} else {
		evt.returnValue = false;
	}
	return false;
}, {
	passive: false
});
document.addEventListener("mousemove", function(e) {
	var evt = e || window.event;		 
	if(evt.preventDefault) {
		evt.preventDefault();
	} else {
		evt.returnValue = false;
	}
	evt.stopPropagation();
	return false;
},{
	passive: false
});
document.addEventListener("mousedown", function(e) {
	var evt = e || window.event;		 
	if(evt.preventDefault) {
		evt.preventDefault();
	} else {
		evt.returnValue = false;
	}
	evt.stopPropagation();
	return false;
},{
	passive: false
});
document.addEventListener("mouseup", function(e) {
	var evt = e || window.event;		 
	if(evt.preventDefault) {
		evt.preventDefault();
	} else {
		evt.returnValue = false;
	}
	evt.stopPropagation();
	return false;
},{
	passive: false
});
document.addEventListener("click", function(e) {
	var evt = e || window.event;
	if(evt.preventDefault) {
		evt.preventDefault();
	} else {
		evt.returnValue = false;
	}
	return false;
}, {
	passive: false
});
//判断移动端
function isMobile() {
	var sUserAgent = navigator.userAgent.toLowerCase(),
		bIsIpad = sUserAgent.match(/ipad/i) == "ipad",
		bIsIphoneOs = sUserAgent.match(/iphone os/i) == "iphone os",
		bIsMidp = sUserAgent.match(/midp/i) == "midp",
		bIsUc7 = sUserAgent.match(/rv:1.2.3.4/i) == "rv:1.2.3.4",
		bIsUc = sUserAgent.match(/ucweb/i) == "ucweb",
		bIsAndroid = sUserAgent.match(/android/i) == "android",
		bIsCE = sUserAgent.match(/windows ce/i) == "windows ce",
		bIsWM = sUserAgent.match(/windows mobile/i) == "windows mobile",
		bIsWebview = sUserAgent.match(/webview/i) == "webview";
	return (
		bIsIpad || bIsIphoneOs || bIsMidp || bIsUc7 || bIsUc || bIsAndroid || bIsCE || bIsWM
	);
}
var touchStart = isMobile() ? "touchstart" : "mousedown";
var touchMove = isMobile() ? "touchmove" : "mousemove";
var touchEnd = isMobile() ? "touchend" : "mouseup";
function UIload(){
	this.cns = 0;
}
$(document).ready(function() {
	loadAssets();
});

function loadAssets() {
	var assets = [
		"audio/2.mp3",
		"audio/2_1.mp3",
		"audio/boom.mp3",
		"audio/handplay.mp3",
		"audio/handplay1.mp3",
		"audio/sound3.mp3",
		"audio/sound3_1.mp3",
		"audio/sound3_2.mp3",
		"audio/sound3_3.mp3",
		"audio/sound3_4.mp3",
		"audio/sound3_5.mp3",
		"audio/sound4.mp3",
		"audio/sound6.mp3",
		"audio/sound7.mp3",
		"audio/sound8.mp3",
		"audio/sound9.mp3",
		"audio/sound10.mp3",
		"audio/sound12.mp3",
		"audio/sound13.mp3",
		"audio/sound14.mp3",
		"audio/sound15.mp3",
		"audio/sound16.mp3",
		"audio/sound17.mp3",
		"audio/sound18.mp3",
		"audio/sound19.mp3",
		"audio/sound20.mp3",
		"audio/sound21.mp3",
		"audio/success.mp3"
	];
	var preload = new createjs.LoadQueue();
	preload.installPlugin(createjs.Sound); // 加载声音插件
	// preload.on('progress', handleProgress);
	preload.on("complete", handleComplete);
	preload.setMaxConnections(1);

	function handleComplete(e) {
		$("#jindu").hide();
		$("#wrap").fadeIn(300);//关闭loading
		$(".p1_2").attr({"src":"img/1/3.gif"});
	}

	preload.loadManifest(assets, true);
}
//正式内容
$(document).ready(function(){
	//通用变量设置
	var secheight = 0;
	var jindu = $("#jindu");
	var btnmovie = $(".btnmovie");
	var content = document.getElementById("wrap");
	var zuan = $(".zuan");
	var etxt = $(".etxt");
	var eight = 0;
	var timer = null;
	var audioz = $(".auds");
	var dxa = 0;
	var timer5 = null;
	var aaa = 0;
	var note = 0;
	var notebool = true;
	var choset = $(".choset");
	var kongt = $(".kongt");
	var startX = 0;
	var startY = 0;
	var endX = 0;
	var endY = 0;
	var index = 0;
	var zhi = -1;
	var zhi2 = -1;
	var zhi3 = -1;
	var num = 0;
	var num2 = 0;
	var num3 = 0;
	var cns = 0;
	var resety = [69, 25, 33.5, 43, 52, 52,33,60,25,43,60];
	var resetx = [59, 58, 77, 58, 58.5, 72,58,59,70,72,71];
	var resetx2 = 59;
	var resety2 = [29, 40, 51, 62, 73, 84];
	var resetx3 = 40;
	var resety3 = [20, 40, 51, 62, 73];
	var eindex = 0;
	var zuan = $(".zuan");
	var etxt = $(".etxt");
	var eight = 0;
	var a = 0;
	var fns = 0;
	var lns = 0;
	var nubs = 0;
	var timergif = null;
	var dx1 = $(".sixbtn");
	//声音变量
	var audio_hand = $(".audio_hand")[0];
	var audio_hand1 = $("audio_hand1")[0];
	var aud1 = $(".audio_1");
	var aud2 = $(".audio_2");
	var aud3 = $(".audio_3");
	var aud4 = $(".audio_4");
	var aud5 = $(".audio_5");
	var aud6 = $(".audio_6");
	var aud7 = $(".audio_7");
	var aud8 = $(".audio_8");
	var aud9 = $(".audio_9");
	var aud10 = $(".audio_10");
	var aud11 = $(".audio_11");
	var aud12 = $(".audio_12");
	// var aud13 = $(".audio_13")[0];
	var aud14 = $(".audio_14");
	var aud15 = $(".audio_15");
	var aud16 = $(".audio_16");
	var aud17 = $(".audio_17");
	var aud18 = $(".audio_18");
	var aud19 = $(".audio_19");
	var aud20 = $(".audio_20");
	var aud21 = $(".audio_21");
	var aud22 = $(".audio_22");
	var aud23 = $(".audio_23");
	var aud24 = $(".audio_24");
	var aud25 = $(".audio_25");
	var sucess = $(".success");
	var boom = $(".boom");
	var aduarr = [aud1,aud2,aud3,aud4,aud5,aud6,aud7,aud8,aud9,aud10,aud11,aud12,aud14,aud15,aud16,aud17,aud18,aud19,aud20,aud21,aud22,aud23,aud24,aud25];

	//loading进度条
	//$("#jindu").show();
	//$("#wrap").fadeIn(300);

	//应用方法
	$("#jindu").show();
	//停止音频播放
	function stopaudio() {
		sucess.get(0).pause();
		sucess.get(0).currentTime = 0;
		boom.get(0).pause();
		boom.get(0).currentTime = 0;
		for (var i = 0; i < aduarr.length; i++) {
			aduarr[i].get(0).pause();
			aduarr[i].get(0).currentTime = 0;
		}
	}

	//初始化方法
	function loadStyle(){
		$(".page div").removeAttr("style");
		$(".page img").removeAttr("style");
		$(".page div").removeClass("flashnews");
		$(".page img").removeClass("flashnews");
		$(".page div").removeClass("flashnews1");
		$(".page img").removeClass("flashnews1");
	}


	//主界面加载

	UIload.prototype.loadPage = function(){
		$(".tiziContent img").css({
			opacity:1
		});
		$(".mubanContent img").css({
			opacity:1
		});
		$(".mubanTextContent img").css({
			opacity:1
		});
	};
	UIload.prototype.anmations = function(id,className,time){
		setTimeout(function(){
			$(id).addClass(className);
		 },time);
	};
	
	UIload.prototype.load = function(){
		$("audio").load();
		stopaudio();
		$(".page div").removeAttr("style");
		$(".page img").removeAttr("style");
		$(".page").hide();
		$("#page19").show();
	    $(".p1_2").attr({"src":"img/1/3.gif"}).show();
		UIplay.next11();
		setTimeout(function(){
			$(".p1_1").show().transition({opacity:1},1000);
		},3000);
		
	};
	//场景2
	UIload.prototype.next1 = function(){
		loadStyle();
		clearTimeout(t1);
		clearTimeout(t2);
		clearTimeout(t3);
		clearTimeout(t4);
		clearTimeout(t5);

		$("#page2 img").each(function(){
			$(this).removeAttr("style");
			$(this).removeClass("flashnews1");
		});
		$(".p2_6").removeClass("flashnews");
		stopaudio();
		$("#page2 .p2_27,#page2 .p2_7,#page2 .p2_8,#page2 .p2_9,#page2 .p2_10,#page2 .p2_11,#page2 .p2_12,#page2 .p2_13,#page2 .p2_14,#page2 .p2_15,#page2 .p2_16,#page2 .p2_17,#page2 .p2_18,#page2 .p2_19,#page2 .p2_20,#page2 .p2_21,#page2 .p2_22,#page2 .p2_23,#page2 .p2_24,#page2 .p2_25,#page2 .p2_26").css({opacity:0});
		$("#page1").hide();
		$("#page2").show();
		aud24.get(0).play();

		aud24.bind("ended",function(){
			$("#page2 .p2_27").show().transition({opacity:1},300);
			$("#page2 .p2_28").transition({opacity:1},300);
			$("#prev2").show();
		});
		//动画流程控制
		var t1 = setTimeout(function(){
			$("#page2 .p2_5").transition({opacity:0},500);
		 },4500);
		 var t2 = setTimeout(function(){
			$("#page2 .p2_6").addClass("flashnews");
		 },9400);
		 //梯子发光
		 var t3 = setTimeout(function(){
			$("#page2 .p2_7").transition({opacity:1},300,function(){
				$("#page2 .p2_8").transition({opacity:1},300,function(){
					$("#page2 .p2_9").transition({opacity:1},300,function(){
						$("#page2 .p2_10").transition({opacity:1},300,function(){
							$("#page2 .p2_11").transition({opacity:1},300,function(){
								$("#page2 .p2_7,#page2 .p2_8,#page2 .p2_9,#page2 .p2_10,#page2 .p2_11").removeClass("flashnews1").transition({opacity:0},500);
							});
						});
					});
				});
			});
		 },10500);
		 //锁动画
		 $("#page2 .p2_16,#page2 .p2_15,#page2 .p2_14,#page2 .p2_13,#page2 .p2_12").css({y:-100});
		 var t4 = setTimeout(function(){
			$("#page2 .p2_16").transition({y:0,opacity:1},200,function(){
				$("#page2 .p2_15").transition({y:0,opacity:1},200,function(){
					$("#page2 .p2_14").transition({y:0,opacity:1},200,function(){
						$("#page2 .p2_13").transition({y:0,opacity:1},200,function(){
							$("#page2 .p2_12").transition({y:0,opacity:1},200);
						});
					});
				});
			});
		 },13500);
		 //木板动画
		 $("#page2 .p2_17,#page2 .p2_18,#page2 .p2_19,#page2 .p2_20,#page2 .p2_21").css({y:50});
		 var t5 = setTimeout(function(){
			$("#page2 .p2_21").transition({y:0,opacity:1},400,function(){
				$("#page2 .p2_20").transition({y:0,opacity:1},400,function(){
					$("#page2 .p2_19").transition({y:0,opacity:1},400,function(){
						$("#page2 .p2_18").transition({y:0,opacity:1},400,function(){
							$("#page2 .p2_17").transition({y:0,opacity:1},400);
						});
					});
				});
			});
		 },15000);
		 //木板文字动画
		 var t6 = setTimeout(function(){
			$("#page2 .p2_22").transition({opacity:1},400,function(){
				$("#page2 .p2_23").transition({opacity:1},400,function(){
					$("#page2 .p2_24").transition({opacity:1},400,function(){
						$("#page2 .p2_25").transition({opacity:1},400,function(){
							$("#page2 .p2_26").transition({opacity:1},400);
						});
					});
				});
			});
		 },18000);
		
	};
	//场景3
	UIload.prototype.next2 = function(){
		loadStyle();
		$("#page3 img").each(function(){
			$(this).removeAttr("style");
			$(this).removeClass("drotate");
		});
		$(".p3_1_3,.p3_1_4,.p3_1_5,.p3_1_6,.p3_1_7").attr({"src":"img/3/jinnang.png"});
		$(".p3_1_3,.p3_1_4,.p3_1_5,.p3_1_6,.p3_1_7").removeAttr("data");
		stopaudio();
		$("#page2").hide();
		$("#page3").show();
		aud1.get(0).play();

		$(".p3_1_1").css({y:100});
		$(".p3_1_2").css({x:-100});
		$(".p3_1_8").css({width:0});
		setTimeout(function(){
			$(".p3_1_2").transition({x:0,opacity:1},1000,function(){
				$(".p3_1_8").transition({width:"30%",opacity:1},300,function(){
					$(".p3_1_3,.p3_1_4,.p3_1_5,.p3_1_6,.p3_1_7").css({
						"cursor":"pointer"
					});
				});
			});
		},10000);
		//声音控制
		aud1.bind("ended",function(){

			nubs = 0;

			function jinnanPlay(){
				$(".p3_3").show().transition({opacity:1},400);
				$(".p3_back").transition({opacity:1},400);
				$(".p3_1_3,.p3_1_4,.p3_1_5,.p3_1_6,.p3_1_7,#p3_1 .p3_2_5,#p3_2 .p3_2_5,#p3_3 .p3_2_5,#p3_4 .p3_2_5,#p3_5 .p3_2_5").off(touchStart);
				$("#prev3").show();
				$(".p3_1_3,.p3_1_4,.p3_1_5,.p3_1_6,.p3_1_7").css({
					"cursor":"default"
				});
				$(".p3_1_3,.p3_1_4,.p3_1_5,.p3_1_6,.p3_1_7").removeAttr("data");
			}
			function prevStatus(){
				var prev3Status = $("#page3").attr("data");
				if(prev3Status == 2){
					$("#prev3").hide();
				}
			}
			function prevStatusOpen(){
				// var prev3Status = $("#page3").attr("data");
				// if(prev3Status == 2){
				// 	$("#prev3").show();
				// }
			}
			//点击锦囊
			$("#p3_1,#p3_2,#p3_3,#p3_4,#p3_5").css({scale:0,opacity:0});
			$(".p3_1_3").on(touchStart,function(e){
				prevStatus();
				e.stopPropagation();
				stopaudio();
				aud2.get(0).play();
				if(!$(".p3_1_3").attr("data")){
					nubs = nubs + 1;
					$(".p3_1_3").attr({"data":"1"});
				}
				
				$(".p3_1_3").attr({"src":"img/3/jinnang1.png"});

				$("#p3_1").show().transition({scale:1,opacity:1},300);
					
				aud2.bind("ended",function(){
					$("#p3_1 .p3_2_5").show().addClass("drotate");
				});
			});
					
			$(".p3_1_4").on(touchStart,function(e){
				prevStatus();
				e.stopPropagation();
				stopaudio();
				aud3.get(0).play();
				if(!$(".p3_1_4").attr("data")){
					nubs = nubs + 1;
					$(".p3_1_4").attr({"data":"2"});
				}

				$(".p3_1_4").attr({"src":"img/3/jinnang1.png"});

				$("#p3_2").show().transition({scale:1,opacity:1},300);
				aud3.bind("ended",function(){
					$("#p3_2 .p3_2_5").show().addClass("drotate");
				});
			});
					
			$(".p3_1_5").on(touchStart,function(e){
				prevStatus();
				e.stopPropagation();
				stopaudio();
				aud4.get(0).play();
				if(!$(".p3_1_5").attr("data")){
					nubs = nubs + 1;
					$(".p3_1_5").attr({"data":"1"});
				}

				$(".p3_1_5").attr({"src":"img/3/jinnang1.png"});

				$("#p3_3").show().transition({scale:1,opacity:1},300);
				aud4.bind("ended",function(){
					$("#p3_3 .p3_2_5").show().addClass("drotate");
				});

			});
					
			$(".p3_1_6").on(touchStart,function(e){
				prevStatus();
				e.stopPropagation();
				stopaudio();
				aud5.get(0).play();
				if(!$(".p3_1_6").attr("data")){
					nubs = nubs + 1;
					$(".p3_1_6").attr({"data":"1"});
				}

				$(".p3_1_6").attr({"src":"img/3/jinnang1.png"});
	
				$("#p3_4").show().transition({scale:1,opacity:1},300);
				aud5.bind("ended",function(){
					$("#p3_4 .p3_2_5").show().addClass("drotate");
				});
			});
					
			$(".p3_1_7").on(touchStart,function(e){
				prevStatus();
				e.stopPropagation();
				stopaudio();
				aud6.get(0).play();
				if(!$(".p3_1_7").attr("data")){
					nubs = nubs + 1;
					$(".p3_1_7").attr({"data":"1"});
				}

				$(".p3_1_7").attr({"src":"img/3/jinnang1.png"});

				$("#p3_5").show().transition({scale:1,opacity:1},300);
				aud6.bind("ended",function(){
					$("#p3_5 .p3_2_5").show().addClass("drotate");
				});
			});
			//结束点击锦囊
			//关闭锦囊
			$("#close1").off(touchStart).on(touchStart,function(){
				$("#p3_1 .p3_2_5").hide().removeClass("drotate");
				aud2.get(0).pause();
				prevStatusOpen();
				$("#p3_1").transition({scale:0,opacity:0},300,function(){
					$("#p3_1").hide();
					if(nubs == 5){
						jinnanPlay();
					}
				});
			});
			$("#close2").off(touchStart).on(touchStart,function(){
				$("#p3_2 .p3_2_5").hide().removeClass("drotate");
				aud3.get(0).pause();
				prevStatusOpen();
				$("#p3_2").transition({scale:0,opacity:0},300,function(){
					$("#p3_2").hide();
					if(nubs == 5){
						jinnanPlay();
					}
				});
			});
			$("#close3").off(touchStart).on(touchStart,function(){
				$("#p3_3 .p3_2_5").hide().removeClass("drotate");
				aud4.get(0).pause();
				prevStatusOpen();
				$("#p3_3").transition({scale:0,opacity:0},300,function(){
					$("#p3_3").hide();
					if(nubs == 5){
						jinnanPlay();
					}
				});
			});
			$("#close4").off(touchStart).on(touchStart,function(){
				$("#p3_4 .p3_2_5").hide().removeClass("drotate");
				aud5.get(0).pause();
				prevStatusOpen();
				$("#p3_4").transition({scale:0,opacity:0},300,function(){
					$("#p3_4").hide();
					if(nubs == 5){
						jinnanPlay();
					}
				});
			});
			$("#close5").off(touchStart).on(touchStart,function(){
				$("#p3_5 .p3_2_5").hide().removeClass("drotate");
				stopaudio();
				aud6.get(0).pause();
				prevStatusOpen();
				$("#p3_5").transition({scale:0,opacity:0},300,function(){
					$("#p3_5").hide();
					if(nubs == 5){
						jinnanPlay();
					}
				});
			});
			//结束关闭锦囊
		});

		$(".p3_1_3,.p3_1_4,.p3_1_5,.p3_1_6,.p3_1_7").css({scale:0});
		$(".blackBg").transition({opacity:0.7},500,function(){
			$(".p3_1_1").transition({y:0,opacity:1},1000,function(){
				$(".p3_1_3").transition({scale:1,opacity:1},400,function(){
					$(".p3_1_4").transition({scale:1,opacity:1},400,function(){
						$(".p3_1_5").transition({scale:1,opacity:1},400,function(){
							$(".p3_1_6").transition({scale:1,opacity:1},400,function(){
								$(".p3_1_7").transition({scale:1,opacity:1},400);
							});
						});
					});
				});
			});
		 });

	};
	
	//新增场景4-1
	UIload.prototype.next4_1 = function(){
		loadStyle();
		$("#page4_1 .p4_jiangjie").hide();
		$("#page4_1 .p4_jiangjie1").hide();
		$("#page7 img").removeAttr("style");
		$("#page7").show();
		aud9.get(0).play();
		$(".p7_1_1").css({y:100});
		$(".p7_1_2").css({x:-100});
		$(".p7_1_8").css({width:0});
		$(".p7_1 .p7_1_3,.p7_1 .p7_1_4,.p7_1 .p7_1_5,.p7_1 .p7_1_6,.p7_1 .p7_1_7").css({scale:0});
		$(".p7blackBg").transition({opacity:0.7},500,function(){
			$(".p7_1_1").transition({y:0,opacity:1},1000,function(){
				$(".p7_1_2").transition({x:0,opacity:1},1000,function(){
					$(".p7_1_8").transition({width:"30%",opacity:1},100,function(){
						$(".p7_1 .p7_1_3").transition({scale:1,opacity:1},400,function(){
							$(".p7_1 .p7_1_4").transition({scale:1,opacity:1},400,function(){
								$(".p7_1 .p7_1_5").transition({scale:1,opacity:1},400,function(){
									$(".p7_1 .p7_1_6").transition({scale:1,opacity:1},400,function(){
										$(".p7_1 .p7_1_7").transition({scale:1,opacity:1},400,function(){
											$(".p7_2 .p7_1_7,.p7_2 .p7_1_6,.p7_2 .p7_1_5,.p7_2 .p7_1_4,.p7_2 .p7_1_3").attr({"data":"close"});
										});
									});
								});
							});
						});
					});
				});
			});
		});
		aud9.bind("ended",function(){
			//人物点击
			$(".p7_1 .p7_1_7").off(touchStart).on(touchStart,function(){
				stopaudio();
				sucess.get(0).play();
				$(".p7_1 .p7_1_7").hide();
				$(".p7_2 .p7_1_7,.p7_3 .p7_1_7").css({zIndex:1002});
				$(".p7_3 .p7_1_7").css({y:-50});
				$(".p7_3 .p7_1_7").show().transition({opacity:1,y:0});
				$(".p7_2 .p7_1_7").show().transition({opacity:1,perspective:"500px",rotateY: 180},function(){

					var style1 = $(".p7_2 .p7_1_3").attr("data");
					var style2 = $(".p7_2 .p7_1_4").attr("data");
					var style3 = $(".p7_2 .p7_1_5").attr("data");
					var style4 = $(".p7_2 .p7_1_6").attr("data");

					if(style1 == "close"){
						$(".p7_2 .p7_1_3").show().transition({opacity:1,perspective:"500px",rotateY: 180});
						$(".p7_3 .p7_1_3").css({y:-50});
					    $(".p7_3 .p7_1_3").show().transition({opacity:1,y:0});
					}

					if(style2 == "close"){
						$(".p7_2 .p7_1_4").show().transition({opacity:1,perspective:"500px",rotateY: 180});
						$(".p7_3 .p7_1_4").css({y:-50});
					    $(".p7_3 .p7_1_4").show().transition({opacity:1,y:0});
					}

					if(style3 == "close"){
						$(".p7_2 .p7_1_5").show().transition({opacity:1,perspective:"500px",rotateY: 180});
						$(".p7_3 .p7_1_5").css({y:-50});
					    $(".p7_3 .p7_1_5").show().transition({opacity:1,y:0});
					}

					if(style4 == "close"){
						$(".p7_2 .p7_1_6").show().transition({opacity:1,perspective:"500px",rotateY: 180});
						$(".p7_3 .p7_1_6").css({y:-50});
					    $(".p7_3 .p7_1_6").show().transition({opacity:1,y:0});
					}


					$(".p7_1 .p7_1_6,.p7_1 .p7_1_5,.p7_1 .p7_1_4,.p7_1 .p7_1_3").hide();
					$(".p7_2 .p7_1_7,.p7_2 .p7_1_6,.p7_3 .p7_1_6,.p7_2 .p7_1_5,.p7_3 .p7_1_5,.p7_2 .p7_1_4,.p7_3 .p7_1_4,.p7_2 .p7_1_3,.p7_3 .p7_1_3,.p7_3 .p7_1_7").css({zIndex:1002});
					$(".p7_2 .p7_1_7,.p7_2 .p7_1_6,.p7_2 .p7_1_5,.p7_2 .p7_1_4,.p7_2 .p7_1_3").attr({"data":"close"});

					$(".p7_back").transition({opacity:1},500);
					$(".p7_4").show().transition({opacity:1},500);
					$("#prev7").show();
					$(".p7_1 .p7_1_7,.p7_1 .p7_1_6,.p7_1 .p7_1_5,.p7_1 .p7_1_4,.p7_1 .p7_1_3,.p7_1 .p7_1_7").off(touchStart);
				});

			});
			$(".p7_1 .p7_1_6").on(touchStart,function(){
				$(".p7_1 .p7_1_6").hide();
				$(".p7_2 .p7_1_6,.p7_3 .p7_1_6").css({zIndex:1002});
				$(".p7_3 .p7_1_6").css({y:-50});
				$(".p7_2 .p7_1_6").attr({data:"open"});
				$(".p7_2 .p7_1_6").show().transition({opacity:1,perspective:"500px",rotateY: 180});
				$(".p7_3 .p7_1_6").show().transition({opacity:1,y:0});
			});
			$(".p7_1 .p7_1_5").on(touchStart,function(){
				$(".p7_1 .p7_1_5").hide();
				$(".p7_2 .p7_1_5,.p7_3 .p7_1_5").css({zIndex:1002});
				$(".p7_3 .p7_1_5").css({y:-50});
				$(".p7_2 .p7_1_5").attr({data:"open"});
				$(".p7_2 .p7_1_5").show().transition({opacity:1,perspective:"500px",rotateY: 180});
				$(".p7_3 .p7_1_5").show().transition({opacity:1,y:0});
			});
			$(".p7_1 .p7_1_4").on(touchStart,function(){
				$(".p7_1 .p7_1_4").hide();
				$(".p7_2 .p7_1_4,.p7_3 .p7_1_4").css({zIndex:1002});
				$(".p7_3 .p7_1_4").css({y:-50});
				$(".p7_2 .p7_1_4").attr({data:"open"});
				$(".p7_2 .p7_1_4").show().transition({opacity:1,perspective:"500px",rotateY: 180});
				$(".p7_3 .p7_1_4").show().transition({opacity:1,y:0});
			});
			$(".p7_1 .p7_1_3").on(touchStart,function(){
				$(".p7_1 .p7_1_3").hide();
				$(".p7_2 .p7_1_3,.p7_3 .p7_1_3").css({zIndex:1002});
				$(".p7_3 .p7_1_3").css({y:-50});
				$(".p7_2 .p7_1_3").attr({data:"open"});
				$(".p7_2 .p7_1_3").show().transition({opacity:1,perspective:"500px",rotateY: 180});
				$(".p7_3 .p7_1_3").show().transition({opacity:1,y:0});
			});
		 });
	};
	//场景4
	UIload.prototype.next3_1 = function(){
		$(".p4_1").hide();
		$("#page4").hide();
		$("#page4_1").show();
		$("#prev23").hide();
		aud8.get(0).play();
		$(".p4_1").hide();
		$(".p4_hand").hide();
		aud8.bind("ended",function(){
			$(".p2_16_b").show().css({
				"cursor":"pointer"
			});
			$(".p2_16").transition({opacity:0},300,function(){
				$(".hand,#page4_1 .p2_7").show().addClass("flashnews1");
			});	
			$("#prev30").show();
			$(".p2_16_b").off(touchStart).on(touchStart,function(){
				$(".p2_16_b").hide();
				$("#prev7").hide();
				$("#prev30").hide();
				$("#page4 .p2_1,#page4 .p2_2").removeClass("bounceInDown");
				UIplay.next4_1();
			}); 
		});
	};
	UIload.prototype.next3 = function(){
		$("#page4 .p2_1,#page4 .p2_2").addClass("bounceInDown");
		loadStyle();
		$(".p4_1").off(touchStart);

		$("#page4 img").each(function(){
			$(this).removeAttr("style");
			$(this).removeClass("flashnews1");
		});
		$("#page7 img").each(function(){
			$(this).removeAttr("style");
		});
		stopaudio();
		$("#page4 .suoContent").show();
		$("#page4 .tiziContent").show();
		$("#page4 .p2_16,#page4 .p2_15,#page4 .p2_14,#page4 .p2_13,#page4 .p2_12").css({y:-100,opacity:0});
		$("#page4 .p2_2").css({left:"85%"});
		$("#page4 .p2_2,#page4 .p2_1").addClass("bounceInDown");
		
		$("#page3,.page20").hide();
		$("#page4").show();
		$("#page4 .p2_16_b,#page4 .p2_15_b,#page4 .p2_14_b,#page4 .p2_13_b,#page4 .p2_12_b").hide();
		$(".hand,#page4 .p2_7").hide().removeClass("flashnews1");
		$(".p4_back,#page4 .p2_7,#page4 .p2_8,#page4 .p2_9,#page4 .p2_10,#page4 .p2_11,#page4 .p2_12,#page4 .p2_13,#page4 .p2_14,#page4 .p2_15,#page4 .p2_16,#page4 .p2_17,#page4 .p2_18,#page4 .p2_19,#page4 .p2_20,#page4 .p2_21,#page4 .p2_22,#page4 .p2_23,#page4 .p2_24,#page4 .p2_25,#page4 .p2_26").css({opacity:0});
		$(".p4_jiangjie").hide();
		$(".p4_jiangjie1").hide();
		aud7.get(0).play();
		$(".audio_7").bind("ended",function(){
			//aud25.play();
		});
		$(".audio_25").bind("ended",function(){
			$("#page4 .p2_27").show().transition({opacity:1},300);
		});
		//动画流程控制
		    setTimeout(function(){
			    $("#page4 .p2_5").transition({opacity:0},500);
			 },1500);
			 setTimeout(function(){
			$("#page4 .p2_6").show().addClass("flashnews");
			 },1500);
			 
			 //梯子发光
			 setTimeout(function(){
			$("#page4 .p2_7").show().transition({opacity:1},200,function(){
				$("#page4 .p2_8").show().transition({opacity:1},200,function(){
					$("#page4 .p2_9").show().transition({opacity:1},200,function(){
						$("#page4 .p2_10").show().transition({opacity:1},200,function(){
							$("#page4 .p2_11").show().transition({opacity:1},200,function(){
								$("#page4 .p2_7,#page4 .p2_8,#page4 .p2_9,#page4 .p2_10,#page4 .p2_11").removeClass("flashnews1").transition({opacity:0},500);
							});
						});
					});
				});
			});
			 },2500);
			 //锁动画
			
			 setTimeout(function(){
			$("#page4 .p2_16").transition({y:0,opacity:1},200,function(){
				$("#page4 .p2_15").transition({y:0,opacity:1},200,function(){
					$("#page4 .p2_14").transition({y:0,opacity:1},200,function(){
						$("#page4 .p2_13").transition({y:0,opacity:1},200,function(){
							$("#page4 .p2_12").transition({y:0,opacity:1},200);
						});
					});
				});
			});
			 },3500);
			 //木板动画
			 $("#page4 .p2_17,#page4 .p2_18,#page4 .p2_19,#page4 .p2_20,#page4 .p2_21").css({y:50});
			 setTimeout(function(){
			$("#page4 .p2_21").transition({y:0,opacity:1},200,function(){
				$("#page4 .p2_20").transition({y:0,opacity:1},200,function(){
					$("#page4 .p2_19").transition({y:0,opacity:1},200,function(){
						$("#page4 .p2_18").transition({y:0,opacity:1},200,function(){
							$("#page4 .p2_17").transition({y:0,opacity:1},200);
						});
					});
				});
			});
			 },5000);
			 //木板文字动画
			 setTimeout(function(){
			$("#page4 .p2_22").transition({opacity:1},200,function(){
				$("#page4 .p2_23").transition({opacity:1},200,function(){
					$("#page4 .p2_24").transition({opacity:1},200,function(){
						$("#page4 .p2_25").transition({opacity:1},200,function(){
							$("#page4 .p2_26").transition({opacity:1},200);
						});
					});
				});
			});
			 },6500);
			 //气球
			 $(".p4_1").css({y:100});
			 setTimeout(function(){
			$(".p4_1").show().transition({y:0,opacity:1},1000,function(){
				$("#prev23").show();
			});
			
			 },8000);
			 setTimeout(function(){
					
			$(".p4_1").css({
				"cursor":"pointer"
			});
			$(".p4_hand").show();
			//第一步
			$(".p4_1").off(touchStart).on(touchStart,function(){
				$(".p4_1").hide();
				//  $(".p4_back").show().css({opacity:1});
				UIplay.next3_1();							
				
			});	
			 },10000);
	};

	//场景4-2
	UIload.prototype.next4_2 = function(){
		$("#page9 img").removeAttr("style");
		$(".hand2").removeClass("flashnews1");
		stopaudio();
		$("#page9").show();
		$("#prev28").hide();
		aud11.get(0).play();
		$(".p9_1_1").css({y:100});
		$(".p9_1_2").css({x:-100});
		$(".p9_1_11").css({width:0});
		$(".p9blackBg").transition({opacity:0.7},500,function(){
			$(".p9_1_1").transition({y:0,opacity:1},1000,function(){
				$(".p9_1_3,.p9_1_4,.p9_1_5,.p9_1_6,.p9_1_7,.p9_1_8,.p9_1_9,.p9_1_10").show().transition({
					opacity:1
				},500);
			});
		});
		aud11.bind("ended",function(){
			$(".p9_1_2").transition({x:0,opacity:1},1000,function(){
				$(".p9_1_11").transition({width:"40%",opacity:1},300,function(){});
				$(".p9_1_3,.p9_1_4,.p9_1_5,.p9_1_6,.p9_1_7,.p9_1_8,.p9_1_9,.p9_1_10").css({"cursor": "pointer"});
			});
			//点击地雷
			var lei = 0;
			$(".p9_1_3").off(touchStart).on(touchStart,function(){
				stopaudio();
				lei = lei + 1;
				$(".p9_2_3").css({opacity:0,zIndex:1002});
				boom.get(0).play();
				$(".p9_1_3").hide();
				$(".p9_2_3").transition({opacity:1},200,function(){
					if(lei == 8){
						$(".p9_4").show().transition({opacity:1},400);
						$("#prev9").show();
						$(".p9_1_3,.p9_1_4,.p9_1_5,.p9_1_6,.p9_1_7,.p9_1_8,.p9_1_9,.p9_1_10").off(touchStart);
					}
				});
			});
			$(".p9_1_4").off(touchStart).on(touchStart,function(){
				stopaudio();
				lei = lei + 1;
				$(".p9_2_4").css({opacity:0,zIndex:1002});
				boom.get(0).play();
				$(".p9_1_4").hide();
				$(".p9_2_4").transition({opacity:1},200,function(){
					if(lei == 8){
						$(".p9_4").show().transition({opacity:1},400);
						$("#prev9").show();
						$(".p9_1_3,.p9_1_4,.p9_1_5,.p9_1_6,.p9_1_7,.p9_1_8,.p9_1_9,.p9_1_10").off(touchStart);
					}
				});
			});
			$(".p9_1_5").off(touchStart).on(touchStart,function(){
				stopaudio();
				lei = lei + 1;
				$(".p9_2_5").css({opacity:0,zIndex:1002});
				sucess.get(0).play();
				$(".p9_1_5").hide();
				$(".p9_2_5").transition({opacity:1},200,function(){
					if(lei == 8){
						$(".p9_4").show().transition({opacity:1},400);
						$("#prev9").show();
						$(".p9_1_3,.p9_1_4,.p9_1_5,.p9_1_6,.p9_1_7,.p9_1_8,.p9_1_9,.p9_1_10").off(touchStart);
					}
				});
			});
			$(".p9_1_6").off(touchStart).on(touchStart,function(){
				stopaudio();
				lei = lei + 1;
				$(".p9_2_6").css({opacity:0,zIndex:1002});
				boom.get(0).play();
				$(".p9_1_6").hide();
				$(".p9_2_6").transition({opacity:1},200,function(){
					if(lei == 8){
						$(".p9_4").show().transition({opacity:1},400);
						$("#prev9").show();
						$(".p9_1_3,.p9_1_4,.p9_1_5,.p9_1_6,.p9_1_7,.p9_1_8,.p9_1_9,.p9_1_10").off(touchStart);
					}
				});
			});
			$(".p9_1_7").off(touchStart).on(touchStart,function(){
				stopaudio();
				lei = lei + 1;
				$(".p9_2_7").css({opacity:0,zIndex:1002});
				boom.get(0).play();
				$(".p9_1_7").hide();
				$(".p9_2_7").transition({opacity:1},200,function(){
					if(lei == 8){
						$(".p9_4").show().transition({opacity:1},400);
						$("#prev9").show();
						$(".p9_1_3,.p9_1_4,.p9_1_5,.p9_1_6,.p9_1_7,.p9_1_8,.p9_1_9,.p9_1_10").off(touchStart);
					}
				});
			});
			$(".p9_1_8").off(touchStart).on(touchStart,function(){
				stopaudio();
				lei = lei + 1;
				$(".p9_2_8").css({opacity:0,zIndex:1002});
				boom.get(0).play();
				$(".p9_1_8").hide();
				$(".p9_2_8").transition({opacity:1},200,function(){
					if(lei == 8){
						$(".p9_4").show().transition({opacity:1},400);
						$("#prev9").show();
						$(".p9_1_3,.p9_1_4,.p9_1_5,.p9_1_6,.p9_1_7,.p9_1_8,.p9_1_9,.p9_1_10").off(touchStart);
					}
				});
			});
			$(".p9_1_9").off(touchStart).on(touchStart,function(){
				stopaudio();
				lei = lei + 1;
				$(".p9_2_9").css({opacity:0,zIndex:1002});
				sucess.get(0).play();
				$(".p9_1_9").hide();
				$(".p9_2_9").transition({opacity:1},200,function(){
					if(lei == 8){
						$(".p9_4").show().transition({opacity:1},400);
						$("#prev9").show();
						$(".p9_1_3,.p9_1_4,.p9_1_5,.p9_1_6,.p9_1_7,.p9_1_8,.p9_1_9,.p9_1_10").off(touchStart);
					}
				});
			});
			$(".p9_1_10").off(touchStart).on(touchStart,function(){
				stopaudio();
				lei = lei + 1;
				$(".p9_2_10").css({opacity:0,zIndex:1002});
				boom.get(0).play();
				$(".p9_1_10").hide();
				$(".p9_2_10").transition({opacity:1},200,function(){
					if(lei == 8){
						$(".p9_4").show().transition({opacity:1},400);
						$("#prev9").show();
						$(".p9_1_3,.p9_1_4,.p9_1_5,.p9_1_6,.p9_1_7,.p9_1_8,.p9_1_9,.p9_1_10").off(touchStart);
					}
				});
			});
		 });
	};
	//场景8
	UIload.prototype.next4 = function(){
		$(".p2_15,.p2_6").show().css({opacity:1});
		$(".hand1,.p2_8").removeClass("flashnews1");
		$("#page9 img").each(function(){
			$(this).removeAttr("style");
		});
		$(".p2_14,.p2_13,.p2_12").show().css({opacity:1});
		UIplay.loadPage();
		stopaudio();
		$(".p2_16_b,.p2_15_b,.p2_14_b,.p2_13_b,.p2_12_b").hide();
		aud10.get(0).play();
		$(".p4_jiangjie").show().css({opacity:0,x:50});
		$(".p4_jiangjie").transition({x:0,opacity:1},300);
		$(".p4_jiangjie1").hide();
		aud10.bind("ended",function(){
			$(".p4_jiangjie").hide();
			$(".p4_jiangjie1").show();
			$(".p2_15_b").show().css({
				"cursor":"pointer"
			});
			$(".p2_15").transition({opacity:0},100);
			$(".hand1,.p2_8").show().addClass("flashnews1");
			$("#prev28").show();
		});
		$("#page4 .p2_2,#page4 .p2_1").removeClass("bounceInDown");
		$("#page7").hide();
		$(".hand").hide();
		$("#page4 .p2_7").hide();
		$("#page4 .p2_2").css({left:"75%"});
		$(".p2_15_b").on(touchStart,function(){
			UIplay.next4_2();
		 });

	};
	//场景11-1
	UIload.prototype.next5_1 = function(){
		stopaudio();
		function leiplay(){
			$("#page11 .caidai2").show();
			$(".p11_1_7,.p11_1_8,.p11_1_6,.p11_1_5,.p11_1_4,.p11_1_3").off(touchStart);
			$(".p11_1_3,.p11_1_4,.p11_1_5,.p11_1_6,.p11_1_7,.p11_1_8").css({
				"cursor": "default"
			});
			sucess.get(0).play();
			sucess.bind("ended",function(){
				$(".p11_4").show().transition({opacity:1},400);
				$("#page11 .backp11").show();
			})
		}
		$("#prev27").hide();
		$("#page11 img").removeAttr("style");
		$("#page11").show();
		// aud13.get(0).play();//此处少一个音频
		$(".p11_1_1").css({y:100});
		$(".p11_1_2").css({x:-100});
		$(".p11_1_9").css({width:0});
		$(".p11blackBg").transition({opacity:0.7},500,function(){
			$(".p11_1_1").transition({y:0,opacity:1},1000,function(){
				$(".p11_1_2").transition({x:0,opacity:1},1000,function(){
					$(".p11_1_9").transition({width:"40%",opacity:1},300,function(){
						$(".p11_1_3,.p11_1_4,.p11_1_5,.p11_1_6,.p11_1_7,.p11_1_8").transition({
							opacity:1
						},500);
						$(".p11_1_3,.p11_1_4,.p11_1_5,.p11_1_6,.p11_1_7,.p11_1_8").css({
							"cursor": "pointer"
						});
						//点击地雷
                        setTimeout(function(){
							var ns = 0;
							$(".p11_1_3").on(touchStart,function(){
								console.info(ns)
								ns = ns + 1;
								$(".p11_2_3").css({scale:0,zIndex:1002});
								$(".p11_2_3").transition({scale:1,opacity:1},200,function(){
									if(ns == 6){
										leiplay();
									}
								});
								$(".p11_1_3").off(touchStart);
							});
							$(".p11_1_4").on(touchStart,function(){
								ns = ns + 1;
								$(".p11_2_4").css({scale:0,zIndex:1002});
								$(".p11_2_4").transition({scale:1,opacity:1},200,function(){
									if(ns == 6){
										leiplay();
									}
								});
								$(".p11_1_4").off(touchStart);
							});
							$(".p11_1_5").on(touchStart,function(){
								ns = ns + 1;
								$(".p11_2_5").css({scale:0,zIndex:1002});
								$(".p11_2_5").transition({scale:1,opacity:1},200,function(){
									if(ns == 6){
										leiplay();
									}
								});
								$(".p11_1_5").off(touchStart);
							});
							$(".p11_1_6").on(touchStart,function(){
								ns = ns + 1;
								$(".p11_2_6").css({scale:0,zIndex:1002});
								$(".p11_2_6").transition({scale:1,opacity:1},200,function(){
									if(ns == 6){
										leiplay();
									}
								});
								$(".p11_1_6").off(touchStart);
							});
							$(".p11_1_7").on(touchStart,function(){
								ns = ns + 1;
								$(".p11_2_7").css({scale:0,zIndex:1002});
								$(".p11_2_7").transition({scale:1,opacity:1},200,function(){
									if(ns == 6){
										leiplay();
									}
								});
								$(".p11_1_7").off(touchStart);
							});
							$(".p11_1_8").on(touchStart,function(){
								ns = ns + 1;
								$(".p11_2_8").css({scale:0,zIndex:1002});
								$(".p11_2_8").transition({scale:1,opacity:1},200,function(){
									if(ns == 6){
										leiplay();
									}
								});
								$(".p11_1_8").off(touchStart);
							});
						},700)
						
					});
				});
			});
		});
		
	};
	//场景11
	UIload.prototype.next5 = function(){
		$("#prev28").hide();
		UIplay.loadPage();
		$(".hand2,.p2_9").removeClass("flashnews1");
		$(".p2_14,.p2_12,.p2_13").show().css({opacity:1});
		$("#page11 img").each(function(){
			$(this).removeAttr("style");
		});
		stopaudio();
		$(".p2_16_b,.p2_15_b,.p2_14_b,.p2_13_b,.p2_12_b").hide();
		aud12.get(0).play();
		$(".p4_jiangjie").show();
		$(".p4_jiangjie1").hide();
		aud12.bind("ended",function(){
			$(".p4_jiangjie").hide();
			$(".p4_jiangjie1").show();
			$(".p2_14_b").show().css({
				"cursor":"pointer"
			});
			$(".p2_14").show().transition({opacity:0},100);
			$(".hand2,.p2_9").show().addClass("flashnews1");
			$("#prev27").show();
		});
		$("#page4 .p2_2,#page4 .p2_1").removeClass("bounceInDown");
		$("#page9").hide();
		$(".hand1").hide();
		$(".p2_8").hide();
		$("#page4 .p2_2").transition({left:"60%"},400,function(){
			
		});
		$(".p2_14_b").on(touchStart,function(){
			UIplay.next5_1();
		});
	
	};

	
	//场景12
	UIload.prototype.next6 = function(){
		$("#page12 img").each(function(){
			$(this).removeAttr("style");
		});
		lns = 0;
		stopaudio();
		$("#page4 .p2_2,#page4 .p2_1").removeClass("bounceInDown");
		$("#page11").hide();
		$("#page12").show();
		stopaudio();
		aud14.get(0).play();
		$(".p4_jiangjie").show();
		$(".p4_jiangjie1").hide();
		setTimeout(function(){
			$(".p12_1_2").transition({x:0,opacity:1},1000,function(){
				$(".p12_1_11").transition({width:"45%",opacity:1},300,function(){

				});
			});
		},5000);
		aud14.bind("ended",function(){
			$(".p4_jiangjie").hide();
			$(".p4_jiangjie1").show();
			$(".p12_1_3,.p12_1_4,.p12_1_5,.p12_1_6,.p12_1_7,.p12_1_8,.p12_1_9,.p12_1_10").css({
				"cursor": "pointer"
			});
			//点击纸牌

			function zhipaiPlay(){
				$(".p12_4").show().transition({opacity:1},400);
				$("#prev12").show();
				$(".p12_1_3,.p12_1_4,.p12_1_5,.p12_1_6,.p12_1_7,.p12_1_8,.p12_1_9,.p12_1_10").off(touchStart);
			}
			$(".p12_1_3").off(touchStart).on(touchStart,function(){
				lns = lns+1;
				$(".p12_1_3").transition({perspective:"500px",rotateY: 180,opacity:0},300);
				$(".p12_2_3").css({opacity:0,zIndex:1002});
				$(".p12_3_3").css({y:-50,zIndex:1002});
				$(".p12_3_3").transition({y:0,opacity:1},300);
				$(".p12_2_3").transition({opacity:1},300,function(){
					if(lns == 6){
						zhipaiPlay();
					}
				});
			});
			$(".p12_1_4").off(touchStart).on(touchStart,function(){
				stopaudio();
				boom.get(0).play();
				$(".p12_1_4").transition({perspective:"500px",rotateY: 180,opacity:0},300);
				$(".p12_2_4").css({opacity:0,zIndex:1002});
				$(".p12_3_4").css({y:-50,zIndex:1002});
				$(".p12_3_4").transition({y:0,opacity:1},300);
				$(".p12_2_4").transition({opacity:1},300,function(){
					if(lns == 6){
						zhipaiPlay();
					}
				});
			});
			$(".p12_1_5").off(touchStart).on(touchStart,function(){
				lns = lns+1;
				$(".p12_1_5").transition({perspective:"500px",rotateY: 180,opacity:0},300);
				$(".p12_2_5").css({opacity:0,zIndex:1002});
				$(".p12_3_5").css({y:-50,zIndex:1002});
				$(".p12_3_5").transition({y:0,opacity:1},300);
				$(".p12_2_5").transition({opacity:1},300,function(){
					if(lns == 6){
						zhipaiPlay();
					}
				});
			});
			$(".p12_1_6").off(touchStart).on(touchStart,function(){
				lns = lns+1;
				$(".p12_1_6").transition({perspective:"500px",rotateY: 180,opacity:0},300);
				$(".p12_2_6").css({opacity:0,zIndex:1002});
				$(".p12_3_6").css({y:-50,zIndex:1002});
				$(".p12_3_6").transition({y:0,opacity:1},300);
				$(".p12_2_6").transition({opacity:1},300,function(){
					if(lns == 6){
						zhipaiPlay();
					}
				});
			});
			$(".p12_1_7").off(touchStart).on(touchStart,function(){
				lns = lns+1;
				$(".p12_1_7").transition({perspective:"500px",rotateY: 180,opacity:0},300);
				$(".p12_2_7").css({opacity:0,zIndex:1002});
				$(".p12_3_7").css({y:-50,zIndex:1002});
				$(".p12_3_7").transition({y:0,opacity:1},300);
				$(".p12_2_7").transition({opacity:1},300,function(){
					if(lns == 6){
						zhipaiPlay();
					}
				});
			});
			$(".p12_1_8").off(touchStart).on(touchStart,function(){
				lns = lns + 1;
				$(".p12_1_8").transition({perspective:"500px",rotateY: 180,opacity:0},300);
				$(".p12_2_8").css({opacity:0,zIndex:1002});
				$(".p12_3_8").css({y:-50,zIndex:1002});
				$(".p12_3_8").transition({y:0,opacity:1},300);
				$(".p12_2_8").transition({opacity:1},300,function(){
					if(lns == 6){
						zhipaiPlay();
					}
				});
			});
			$(".p12_1_9").off(touchStart).on(touchStart,function(){
				stopaudio();
				boom.get(0).play();
				$(".p12_1_9").transition({perspective:"500px",rotateY: 180,opacity:0},300);
				$(".p12_2_9").css({opacity:0,zIndex:1002});
				$(".p12_3_9").css({y:-50,zIndex:1002});
				$(".p12_3_9").transition({y:0,opacity:1},300);
				$(".p12_2_9").transition({opacity:1},300,function(){
					if(lns == 6){
						zhipaiPlay();
					}
				});
			});
			$(".p12_1_10").off(touchStart).on(touchStart,function(){
				lns = lns + 1;
				$(".p12_1_10").transition({perspective:"500px",rotateY: 180,opacity:0},300);
				$(".p12_2_10").css({opacity:0,zIndex:1002});
				$(".p12_3_10").css({y:-50,zIndex:1002});
				$(".p12_3_10").transition({y:0,opacity:1},300);
				$(".p12_2_10").transition({opacity:1},300,function(){
					if(lns == 6){
						zhipaiPlay();
					}
				});
			});
		});
		$(".p12_1_1").css({y:100});
		$(".p12_1_2").css({x:-100});
		$(".p12_1_11").css({width:0});
		$(".p12blackBg").transition({opacity:0.7},500,function(){
			$(".p12_1_1").transition({y:0,opacity:1},1000,function(){
				$(".p12_1_3,.p12_1_4,.p12_1_5,.p12_1_6,.p12_1_7,.p12_1_8,.p12_1_9,.p12_1_10").transition({
					opacity:1
				},500);
			});
		});
		
	};
	//场景14-1
	UIload.prototype.next7_1 = function(){
		$("#page14 img").removeAttr("style");
		$("#prev26").hide();
		$("#page14").show();
		stopaudio();
		aud16.get(0).play();
		cns = 0;
		$(".p14_1_1").css({y:100});
		$(".p14_1_2").css({x:-100});
		$(".p14_1_7").css({width:0});
		$(".p14blackBg").transition({opacity:0.7},500,function(){
			$(".p14_1_1").transition({y:0,opacity:1},1000,function(){
				$(".p14_1_2").transition({x:0,opacity:1},1000,function(){
					$(".p14_1_7").transition({width:"45%",opacity:1},300,function(){
						$(".p14_1_3,.p14_1_4,.p14_1_5,.p14_1_6").transition({
							opacity:1
						},500);
						
					});
				});
			});
		});
		aud16.bind("ended",function(){
			$(".p14_1_3,.p14_1_4,.p14_1_5,.p14_1_6").css({
				"cursor": "pointer"
			},500);
			function overPai(){
				$("#page14 .caidai2").show();
				$("#page14 .backp14").show();
				$("#page14 .p14_3").show().css({opacity:1});
				sucess.get(0).play();
				$(".p14_1_3,.p14_1_4,.p14_1_5,.p14_1_6").css({
					"cursor": "default"
				},500);
			}
			//点击纸牌

			$(".p14_1_3").off(touchStart).on(touchStart,function(){
				cns = cns+1;
				$(".p14_2_3").css({zIndex:1002});
				$(".p14_2_3").transition({scale:1,opacity:1},300,function(){
					if(cns == 4){
						overPai();
					}
				});
			});
			$(".p14_1_4").off(touchStart).on(touchStart,function(){
				cns = cns+1;
				$(".p14_2_4").css({zIndex:1002});
				$(".p14_2_4").transition({scale:1,opacity:1},300,function(){
					if(cns == 4){
						overPai();
					}
				});
			});
			$(".p14_1_5").off(touchStart).on(touchStart,function(){
				cns = cns+1;
				$(".p14_2_5").css({zIndex:1002});
				$(".p14_2_5").transition({scale:1,opacity:1},300,function(){
					if(cns == 4){
						overPai();
					}
				});
			});
			$(".p14_1_6").off(touchStart).on(touchStart,function(){
				cns = cns+1;
				$(".p14_2_6").css({zIndex:1002});
				$(".p14_2_6").transition({scale:1,opacity:1},300,function(){
					if(cns == 4){
						overPai();
					}
				});
			});
		});
	};

	//场景14
	UIload.prototype.next7 = function(){
		$("#page4 .p2_12,#page4 .p2_13").show().css({opacity:1});
		$(".hand3,.p2_10").show().removeClass("flashnews1");
		$(".p2_8").hide().removeClass("flashnews1");
		$("#page14 img").each(function(){
			$(this).removeAttr("style");
		});
		UIplay.loadPage();
		stopaudio();
		$(".p2_16_b,.p2_15_b,.p2_14_b,.p2_13_b,.p2_12_b").hide();
		aud15.get(0).play();
		$(".p4_jiangjie").show();
		$(".p4_jiangjie1").hide();
		aud15.bind("ended",function(){
			$(".p4_jiangjie").hide();
			$(".p4_jiangjie1").show();
			$(".p2_13_b").show().css({
				"cursor":"pointer"
			});
			$(".p2_13").transition({opacity:0},100);
			$(".hand3,.p2_10").show().addClass("flashnews1");
			$("#prev26").show();
		});
		$("#page4 .p2_2,#page4 .p2_1").removeClass("bounceInDown");
		$("#page12").hide();
		$(".hand2").hide();
		$(".p2_9").hide();
		$("#page4 .p2_2").transition({left:"45%"},400,function(){
			
		});
		$(".p2_13_b").on(touchStart,function(){
			UIplay.next7_1();
		});

		
		
	};
	//场景15
	UIload.prototype.next8 = function(){
		$("#page15 img").each(function(){
			$(this).removeAttr("style");
		});
		$(".p15_1,.p15_2,.p15_3,.p15_4,.p15_5,.p15_6,.p15_7").hide();
		
		stopaudio();
		aud17.get(0).play();
		$(".p4_jiangjie").show();
		$(".p4_jiangjie1").hide();
		aud17.bind("ended",function(){
			$(".p4_jiangjie").hide();
			$(".p4_jiangjie1").show();
			$(".p15_1,.p15_2,.p15_3,.p15_4,.p15_5,.p15_6,.p15_7").css({
				"cursor":"pointer"
			});
			tz15();
		});
		$("#page14").hide();
		$("#page15").show();
		$("#page15 .kuang").css({y:100});
		$("#page15 .e1").css({x:-100});
		$("#page15 .t1").css({width:0});
		$(".p15blackBg").transition({opacity:0.7},500,function(){
			$("#page15 .kuang").transition({y:0,opacity:1},1000,function(){
				$("#page15 .e1").transition({x:0,opacity:1},1000,function(){
					$("#page15 .t1").transition({width:"45%",opacity:1},300,function(){
						$(".p15_1,.p15_2,.p15_3,.p15_4,.p15_5,.p15_6,.p15_7").show().transition({
							opacity:1
						},500);
						
						$(".kong15").transition({
							opacity:1
						},500);
					});
				});
			});
		});
		  
	};
	//场景17
	UIload.prototype.next9 = function(){
		$("#page16 img").each(function(){
			$(this).removeAttr("style");
		});
		$(".p16_1,.p16_2,.p16_3,.p16_4,.p16_5,.p16_6,.p16_7,.p16_8").hide();
		stopaudio();
		aud18.get(0).play();
		
		$("#page16 .e1").css({x:-100});
		$("#page16 .t1").css({width:0});
		$(".p4_jiangjie").show();
		$(".p4_jiangjie1").hide();
		setTimeout(function(){
			$("#page16 .e1").transition({x:0,opacity:1},1000,function(){
				$("#page16 .t1").transition({width:"45%",opacity:1},300,function(){
	
				});
			});
		},1000);
		aud18.bind("ended",function(){
			$(".p16_1,.p16_2,.p16_3,.p16_4,.p16_5,.p16_6,.p16_7,.p16_8").css({
				"cursor": "pointer"
			});
			$(".p4_jiangjie").hide();
			$(".p4_jiangjie1").show();
			tz16();
			
		});
		$(".kong15").css({
			opacity:0
		});
		$("#page15").hide();
		$("#page16").show();
		$("#page16 .dibu1").css({y:100});
		$("#page16 .dibu2").css({y:100});
		$(".lei").css({y:100});
		$(".tiaozhan").css({y:100});
		setTimeout(function(){
			$(".p16blackBg").transition({opacity:0.7},500,function(){
				$("#page16 .dibu1").transition({y:0,opacity:1},1000,function(){
					$(".tiaozhan").transition({y:0,opacity:1},300);
					$("#page16 .dibu2").transition({y:0,opacity:1},1000,function(){
						$(".lei").transition({y:0,opacity:1},300,function(){
							setTimeout(function(){
								$(".p16_1,.p16_2,.p16_3,.p16_4,.p16_5,.p16_6,.p16_7,.p16_8").show().transition({
									opacity:1
								},300);
							},6000);
						});
					});
				});			
			});
		},1500);
		

	};
	//场景18-1
	UIload.prototype.next10_1 = function(){
		$("#prev25").hide();
		$("#page18 img").each(function(){
			$(this).removeAttr("style");
		});
		
		fns = 0;
		stopaudio();
		$("#page18").show();
		aud20.get(0).play();
		$(".page18_bg .bg18").css({y:100});
		$(".page18_bg .e18").css({x:-100});
		aud20.bind("ended",function(){
			$(".page18_bg .e18").transition({x:0,opacity:1},1000,function(){
				$(".duihuak18").transition({opacity:1},300,function(){
					$(".p18_1,.p18_2,.p18_3,.p18_4,.p18_5,.p18_6,.p18_7,.p18_8").css({
						"cursor":"pointer"
					});
					//点击纸牌
						
					$(".p18_1").off(touchStart).on(touchStart,function(){
						fns = fns+1;
						$(".p18_1").transition({perspective:"500px",rotateY: 180,opacity:0},400);
						$(".p18_1_2").css({x:50});
						$(".p18_1_2").transition({x:0,opacity:1},200);
						$(".p18_1_3").show().transition({opacity:1},200,function(){
							if(fns >= 8){
								$("#page18 .nextk").show().transition({opacity:1},300);
								sucess.get(0).play();
								$(".p18_1,.p18_2,.p18_3,.p18_4,.p18_5,.p18_6,.p18_7,.p18_8").off(touchStart);
								$("#prev18").show();
							}
						});
					});
					$(".p18_2").off(touchStart).on(touchStart,function(){
						fns = fns+1;
						boom.get(0).play();
						$(".p18_2").transition({perspective:"500px",rotateY: 180,opacity:0},400);
						$(".p18_2_2").css({x:50});
						$(".p18_2_2").transition({x:0,opacity:1},200);
						$(".p18_2_3").show().transition({opacity:1},200,function(){
							if(fns >= 8){
								$("#page18 .nextk").show().transition({opacity:1},300);
								sucess.get(0).play();
								$(".p18_1,.p18_2,.p18_3,.p18_4,.p18_5,.p18_6,.p18_7,.p18_8").off(touchStart);
								$("#prev18").show();
							}
						});
					});
					$(".p18_3").off(touchStart).on(touchStart,function(){
						fns = fns+1;
						$(".p18_3").transition({perspective:"500px",rotateY: 180,opacity:0},400);
						$(".p18_3_2").css({x:50});
						$(".p18_3_2").transition({x:0,opacity:1},200);
						$(".p18_3_3").show().transition({opacity:1},200,function(){
							if(fns >= 8){
								$("#page18 .nextk").show().transition({opacity:1},300);
								sucess.get(0).play();
								$("#prev18").show();
								$(".p18_1,.p18_2,.p18_3,.p18_4,.p18_5,.p18_6,.p18_7,.p18_8").off(touchStart);
							}
						});
					});
					$(".p18_4").off(touchStart).on(touchStart,function(){
						stopaudio();
						fns = fns+1;
						boom.get(0).play();
						$(".p18_4").transition({perspective:"500px",rotateY: 180,opacity:0},400);
						$(".p18_4_2").css({x:50});
						$(".p18_4_2").transition({x:0,opacity:1},200);
						$(".p18_4_3").show().transition({opacity:1},200,function(){
							if(fns >= 8){
								$("#page18 .nextk").show().transition({opacity:1},300);
								sucess.get(0).play();
								$("#prev18").show();
								$(".p18_1,.p18_2,.p18_3,.p18_4,.p18_5,.p18_6,.p18_7,.p18_8").off(touchStart);
							}
						});
					});
					$(".p18_5").off(touchStart).on(touchStart,function(){
						fns = fns+1;
						$(".p18_5").transition({perspective:"500px",rotateY: 180,opacity:0},400);
						$(".p18_5_2").css({x:50});
						$(".p18_5_2").transition({x:0,opacity:1},200);
						$(".p18_5_3").show().transition({opacity:1},200,function(){
							if(fns >= 8){
								$("#page18 .nextk").show().transition({opacity:1},300);
								sucess.get(0).play();
								$("#prev18").show();
								$(".p18_1,.p18_2,.p18_3,.p18_4,.p18_5,.p18_6,.p18_7,.p18_8").off(touchStart);
							}
						});
					});
					$(".p18_6").off(touchStart).on(touchStart,function(){
						fns = fns+1;
						$(".p18_6").transition({perspective:"500px",rotateY: 180,opacity:0},400);
						$(".p18_6_2").css({x:50});
						$(".p18_6_2").transition({x:0,opacity:1},200);
						$(".p18_6_3").show().transition({opacity:1},200,function(){
							if(fns >= 8){
								$("#page18 .nextk").show().transition({opacity:1},300);
								sucess.get(0).play();
								$("#prev18").show();
							}
						});
					});
					$(".p18_7").off(touchStart).on(touchStart,function(){
						stopaudio();
						fns = fns+1;
						boom.get(0).play();
						$(".p18_7").transition({perspective:"500px",rotateY: 180,opacity:0},400);
						$(".p18_7_2").css({x:50});
						$(".p18_7_2").transition({x:0,opacity:1},200);
						$(".p18_7_3").show().transition({opacity:1},200,function(){
							if(fns >= 8){
								$("#page18 .nextk").show().transition({opacity:1},300);
								sucess.get(0).play();
								$("#prev18").show();
								$(".p18_1,.p18_2,.p18_3,.p18_4,.p18_5,.p18_6,.p18_7,.p18_8").off(touchStart);
							}
						});
					});

					$(".p18_8").off(touchStart).on(touchStart,function(){
						fns = fns+1;
						$(".p18_8").transition({perspective:"500px",rotateY: 180,opacity:0},400);
						$(".p18_8_2").css({x:50});
						$(".p18_8_2").transition({x:0,opacity:1},200);
						$(".p18_8_3").show().transition({opacity:1},200,function(){
							if(fns >= 8){
								$("#page18 .nextk").show().transition({opacity:1},300);
								sucess.get(0).play();
								$("#prev18").show();
								$(".p18_1,.p18_2,.p18_3,.p18_4,.p18_5,.p18_6,.p18_7,.p18_8").off(touchStart);
							}
						});
					});
				});
			});
		});

		$(".p18blackBg").transition({opacity:0.7},500,function(){
			$(".page18_bg .bg18").transition({y:0,opacity:1},1000,function(){
				$(".p18_1,.p18_2,.p18_3,.p18_4,.p18_5,.p18_6,.p18_7,.p18_8").transition({
					opacity:1
				},500);
			});
		});
	};
	//场景18
	UIload.prototype.next10 = function(){
		$(".p2_12,#hand").show().css({opacity:1});
		$(".hand4,.p2_11").show().removeClass("flashnews1");
		$("#page18 img").each(function(){
			$(this).removeAttr("style");
		});

		stopaudio();
		UIplay.loadPage();
		$(".p2_16_b,.p2_15_b,.p2_14_b,.p2_13_b,.p2_12_b").hide();
		$("#page4 .p2_2,#page4 .p2_1").removeClass("bounceInDown");
		aud19.get(0).play();
		$(".p4_jiangjie").show();
		$(".p4_jiangjie1").hide();
		aud19.bind("ended",function(){
			$(".p4_jiangjie").hide();
			$(".p4_jiangjie1").show();
			$(".p2_12_b,#prev25").show().css({
				"cursor":"pointer"
			});
			$(".p2_12").transition({opacity:0},100);
			$(".hand4,.p2_11").show().addClass("flashnews1");
			
		});

		$("#page16").hide();
		$(".hand3").hide();
		$(".p2_10").hide();
		$("#page4 .p2_2").transition({left:"29%"},400,function(){
			
		});
		$(".p2_12_b").on(touchStart,function(){
			UIplay.next10_1();
		});
	
	};

	//场景19
	UIload.prototype.next11 = function(){

		$(".page20 img").each(function(){
			$(this).removeAttr("style");
		});
		
		$(".page19 .caidai2").removeAttr("style");
		$("#page19 .p2_2").removeAttr("style");
		$("#page19 .p2_2").removeClass("p2_2_1");

		// $(".p20_text_1,.p20_text_2,.p20_text_3,.p20_text_4,.p20_text_5").hide();
		stopaudio();
		$(".p2_16_b,.p2_15_b,.p2_14_b,.p2_13_b,.p2_12_b").hide();
		$("#hand").hide();
		aud21.get(0).play();
		$(".p4_jiangjie").show();
		$(".p4_jiangjie1").hide();
		$("#page18,.suoContent").hide();
		$(".page20").show();
		$(".p20_kong").hide();
		$(".hand4").hide();
		$(".p2_11").hide();
		$(".p2_6").css({opacity:1});
		$(".p2_17,.p2_18,.p2_19,.p2_20,.p2_21").css({opacity:1});

		aud21.bind("ended",function(){
			$(".p4_jiangjie1").show();
			$(".p4_jiangjie").hide();
			$(".p20_text_6").transition({
				opacity:1
			},500,function(){
				
				$(".p20_text_1,.p20_text_2,.p20_text_3,.p20_text_4,.p20_text_5").css({"cursor":"pointer"}).transition({
					opacity:1
				},500);
				$(".p20_kong").show();
				tz20();
				//$("#prev21").show();
			});
		});
	
	};

	//场景20
	UIload.prototype.next12 = function(){
		$("#page22 img").each(function(){
			$(this).removeAttr("style");
		 });
		 $("#page22 .kong").each(function(){
			$(this).addClass("bounceInUp");
		 });
		 $("#page22 .jiantou").each(function(){
			$(this).addClass("fadeInLeft");
		 });
		 $(".page22 .chose").hide();
		
		stopaudio();
		aud23.get(0).play();
		$("#page4").hide();
		$("#page22").show();
		setTimeout(function(){
			$(".p22_e1").css({x:-50});
			$(".p22_e1").transition({x:0,opacity:1},300,function(){
				$(".tipp9_1").transition({opacity:1},300,function(){
					$(".page22 .chose").show().transition({opacity:1},400,function(){
						$(".page22 .chose").css({
							"cursor":"pointer"
						});
						tz22();
					});
				});
			});
			
		},5000);
		
		aud21.bind("ended",function(){
			$(".p4_jiangjie1").show();
			$(".p4_jiangjie").hide();
			$(".p20_text_6").transition({
				opacity:1
			},500);
		});

		function moveSucces(){
			stopaudio();
			sucess.get(0).play();
			$(".page22 .caidai").show();
			$(".page22 .right").show();
			setTimeout(function(){
				$(".page22 .nextp").show();
			},500);

		}
		//移动
		// $(".chose_1").on(touchStart,function(){
		// 	moveSucces();
		// })

	
	};
	//最后的场景
	UIload.prototype.next13 = function(){
		$("#page22").hide();
		$("#page23").show();

		$("#page23 .backp").show();

	};
	


	var UIplay = new UIload();
	//初始化
	setTimeout(function(){
		UIplay.load();
	},500);


	

	//22屏拖拽
	var chose = $(".chose");
	var kong = $(".kong");
	//20屏拖拽
	var chose20 = $(".chose20");
	var kong20 = $(".p20_kong");
	var zhi20 = -1;
	var num20 = 0;
	var resety20 = [21, 21, 21, 21, 21];
	var resetx20 = [23, 36, 48, 62, 75];

	//16屏拖拽
	var chose16 = $(".chose16");
	var kong16 = $(".kong16");
	var zhi16 = -1;
	var num16 = 0;
	var resety16 = [20, 30, 30.5, 20.5, 20,31,20,30];
	var resetx16 = [6, 6, 34, 26, 55,55,74,75];

	//15屏拖拽
	var chose15 = $(".chose15");
	var kong15 = $(".kong15");
	var kuang15 = $(".page15 .kuang");
	var zhi15 = -1;
	var num15 = 0;
	var resety15 = [20, 20, 57, 44, 32,44,32];
	var resetx15 = [40, 65, 40, 65, 65,40,40];
	

	function isCrash(obj1, obj2) {
		// console.info(obj1)
		// console.info(obj2)
		var bool = true;
		//obj1的边界
		var l1 = obj1.offsetLeft;
		var r1 = obj1.offsetLeft + obj1.offsetWidth;
		var t1 = obj1.offsetTop;
		var b1 = obj1.offsetTop + obj1.offsetHeight;
		var l2 = obj2.offsetLeft;
		var r2 = obj2.offsetLeft + obj2.offsetWidth;
		var t2 = obj2.offsetTop;
		var b2 = obj2.offsetTop + obj2.offsetHeight;
		if(l1 < r2 && r1 > l2 && b1 > t2 && b2 > t1) {
			bool = true;
		} else {
			bool = false;
		}
		return bool;
	}

	function one(a) {
		switch(a) {
		case 0:
			{
				chose[a].style.left = 17.5 + "%";
				chose[a].style.top = 9 + "%";
				chose[a].style.cursor = "default";
				num++;
				chose[a].removeEventListener(touchStart, touchstart1, false);
			}
			break;
		case 1:
			{
				chose[a].style.left = 39 + "%";
				chose[a].style.top = 9 + "%";
				chose[a].style.cursor = "default";
				num++;
				chose[a].removeEventListener(touchStart, touchstart1, false);
			}
			break;
		case 2:
			{
				chose[a].style.left = 17.5 + "%";
				chose[a].style.top = 23 + "%";
				chose[a].style.cursor = "default";
				num++;
				chose[a].removeEventListener(touchStart, touchstart1, false);
				console.log(num);
			}
			break;
		case 3:
			{
				chose[a].style.left = 32 + "%";
				chose[a].style.top = 24 + "%";
				chose[a].style.cursor = "default";
				num++;
				chose[a].removeEventListener(touchStart, touchstart1, false);
			}
			break;
		case 4:
			{
				chose[a].style.left = 33 + "%";
				chose[a].style.top = 38 + "%";
				chose[a].style.cursor = "default";
				num++;
				chose[a].removeEventListener(touchStart, touchstart1, false);
			}
			break;
		case 5:
			{
				chose[a].style.left = 17.5 + "%";
				chose[a].style.top = 38 + "%";
				chose[a].style.cursor = "default";
				num++;
				chose[a].removeEventListener(touchStart, touchstart1, false);
			}
			break;
		case 6:
			{
				chose[a].style.left = 31 + "%";
				chose[a].style.top = 50.7 + "%";
				chose[a].style.cursor = "default";
				num++;
				chose[a].removeEventListener(touchStart, touchstart1, false);
			}
			break;
		case 7:
			{
				chose[a].style.left = 17.5 + "%";
				chose[a].style.top = 51 + "%";
				chose[a].style.cursor = "default";
				num++;
				chose[a].removeEventListener(touchStart, touchstart1, false);
			}
			break;
		case 8:
			{
				chose[a].style.left = 17.5 + "%";
				chose[a].style.top = 76 + "%";
				chose[a].style.cursor = "default";
				num++;
				chose[a].removeEventListener(touchStart, touchstart1, false);
			}
			break;
		case 9:
			{
				chose[a].style.left = 37 + "%";
				chose[a].style.top = 67.5 + "%";
				chose[a].style.cursor = "default";
				num++;
				chose[a].removeEventListener(touchStart, touchstart1, false);
			}
			break;
		case 10:
			{
				chose[a].style.left = 17.5 + "%";
				chose[a].style.top = 67 + "%";
				chose[a].style.cursor = "default";
				num++;
				chose[a].removeEventListener(touchStart, touchstart1, false);
			}
			break;
		}
	}
	function size20(a) {
		switch(a) {
		case 0:
			{
				$(".page20 .p20_text_1").hide();
				    $(".p2_22").show().css({opacity:1});
				// num20 = num20 + 1;
			}
			break;
		case 1:
			{
				$(".page20 .p20_text_2").hide();
				$(".p2_23").show().css({opacity:1});
				// num20 = num20 + 1;
			}
			break;
		case 2:
			{
				$(".page20 .p20_text_3").hide();
				$(".p2_24").show().css({opacity:1});
				// num20 = num20 + 1;
			}
			break;
		case 3:
			{
				$(".page20 .p20_text_4").hide();
				$(".p2_25").show().css({opacity:1});
				// num20 = num20 + 1;
			}
			break;
		case 4:
			{
				$(".page20 .p20_text_5").hide();
				$(".p2_26").show().css({opacity:1});
				// num20 = num20 + 1;
			}
			break;
		}
		console.info("数字:"+num20);
	}
	function size16(a) {
		switch(a) {
		case 0:
			{
				chose16[a].style.left = 16 + "%";
				chose16[a].style.top = 49 + "%";
				num16++;
				chose16[a].removeEventListener(touchStart, touchstart16, false);
			}
			break;
		case 1:
			{
				chose16[a].style.left = 55 + "%";
				chose16[a].style.top = 58 + "%";
				num16++;
				chose16[a].removeEventListener(touchStart, touchstart16, false);
			}
			break;
		case 2:
			{
				chose16[a].style.left = 16 + "%";
				chose16[a].style.top = 59 + "%";
				num16++;
				chose16[a].removeEventListener(touchStart, touchstart16, false);
				console.log(num16);
			}
			break;
		case 3:
			{
				chose16[a].style.left = 60 + "%";
				chose16[a].style.top = 80 + "%";
				num16++;
				chose16[a].removeEventListener(touchStart, touchstart16, false);
			}
			break;
		case 4:
			{
				chose16[a].style.left = 16 + "%";
				chose16[a].style.top = 70 + "%";
				num16++;
				chose16[a].removeEventListener(touchStart, touchstart16, false);
			}
			break;
		case 5:
			{
				chose16[a].style.left = 60 + "%";
				chose16[a].style.top = 70 + "%";
				num16++;
				chose16[a].removeEventListener(touchStart, touchstart16, false);
			}
			break;
		case 6:
			{
				chose16[a].style.left = 55 + "%";
				chose16[a].style.top = 50 + "%";
				num16++;
				chose16[a].removeEventListener(touchStart, touchstart16, false);
			}
			break;
		case 7:
			{
				chose16[a].style.left = 75 + "%";
				chose16[a].style.top = 50 + "%";
				num16++;
				chose16[a].removeEventListener(touchStart, touchstart16, false);
			}
			break;
		}
	}
	function size15(a) {
		switch(a) {
		case 0:
			{
				chose15[a].style.left = 8.6 + "%";
				chose15[a].style.top = 18 + "%";
				num15++;
				chose15[a].removeEventListener(touchStart, touchstart15, false);
			}
			break;
		case 1:
			{
				chose15[a].style.left = 8.2 + "%";
				chose15[a].style.top = 44 + "%";
				num15++;
				chose15[a].removeEventListener(touchStart, touchstart15, false);
			}
			break;
		case 2:
			{
				chose15[a].style.left = 8.2 + "%";
				chose15[a].style.top = 57.5 + "%";
				num15++;
				chose15[a].removeEventListener(touchStart, touchstart15, false);
				console.log(num15);
			}
			break;
		case 3:
			{
				chose15[a].style.left = 7.8 + "%";
				chose15[a].style.top = 31 + "%";
				num15++;
				chose15[a].removeEventListener(touchStart, touchstart15, false);
			}
			break;
		case 4:
			{
				chose15[a].style.left = 8.2 + "%";
				chose15[a].style.top = 70 + "%";
				num15++;
				chose15[a].removeEventListener(touchStart, touchstart15, false);
			}
			break;
		case 5:
			{
				chose15[a].style.left = 60 + "%";
				chose15[a].style.top = 70 + "%";
				chose15[a].removeEventListener(touchStart, touchstart15, false);
			}
			break;
		case 6:
			{
				chose15[a].style.left = 55 + "%";
				chose15[a].style.top = 50 + "%";
				chose15[a].removeEventListener(touchStart, touchstart15, false);
			}
			break;
		}
	}
	
	function touchstart1(e) {
		var Event = e || window.event;
		var otouch;
		if(Event.touches) {
			otouch = Event.touches[0];
		} else {
			otouch = Event;
		}
		if(num >= 5) {
			$(".ring").load();
		}
		startX = otouch.clientX;
		startY = otouch.clientY;
		zhi = this.index;
		// console.log(zhi);
		// console.log(a);
		//阻止默认事件,必须加的
		document.addEventListener(touchMove, touchMove1, false);
		return false;
	}
	function touchMove1(e) {
		var Event1 = e || window.event;
		var otouch;
		var bodyleft = (document.documentElement.clientWidth - content.clientWidth)/2;
		var bodyh = content.clientHeight-20;
		var bodyw = content.clientWidth + bodyleft;
		var bodyh2 = 20;		
		if(Event1.touches) {
			otouch = Event1.touches[0];
			//console.log(1)
		} else {
			otouch = Event1;
			console.log(2);
		}
		endX = otouch.clientX;
		endY = otouch.clientY;
		// console.log(bodyh+"hhhh");
		// console.log(bodyw+"wwww");
		// console.log(endX+"endx");
		if(endX<bodyleft||endX>=bodyw||endY<bodyh2||endY>=bodyh){
			document.removeEventListener(touchMove,touchMove1);
			chose[zhi].style.top = resety[zhi] + "%";
			chose[zhi].style.left = resetx[zhi] + "%";
		}else{
			chose[zhi].style.left = chose[zhi].offsetLeft + endX - startX + "px";
			chose[zhi].style.top = chose[zhi].offsetTop + endY - startY + "px";
		}
		
		startX = endX;
		startY = endY;
		//阻止默认事件,必须加的
		Event1.preventDefault();
		return false;
	}
	function touchend1() {
		// console.log(num);
		document.removeEventListener(touchMove, touchMove1);
		document.removeEventListener(touchStart, touchstart1);
		// console.info(isCrash(chose[zhi], kong[1]))
		if(isCrash(chose[zhi], kong[0])&&zhi == 0) {
			if(zhi == 0) {
				one(zhi);
			} else {
				chose[zhi].style.top = resety[zhi] + "%";
				chose[zhi].style.left = resetx[zhi] + "%";
			}
		} else if(isCrash(chose[zhi], kong[0]) && zhi == 1) {
			if(zhi == 1) {
				one(zhi);
			} else {
				chose[zhi].style.top = resety[zhi] + "%";
				chose[zhi].style.left = resetx[zhi] + "%";
			}
		} else if(isCrash(chose[zhi], kong[1]) && zhi == 2) {
			if(zhi == 2) {
				one(zhi);
			} else {
				chose[zhi].style.top = resety[zhi] + "%";
				chose[zhi].style.left = resetx[zhi] + "%";
			}
		} else if(isCrash(chose[zhi], kong[1]) && zhi == 3) {
			if(zhi == 3) {
				one(zhi);
			} else {
				chose[zhi].style.top = resety[zhi] + "%";
				chose[zhi].style.left = resetx[zhi] + "%";
			}
		}else if(isCrash(chose[zhi], kong[2]) && zhi == 4) {
			if(zhi == 4) {
				one(zhi);
			} else {
				chose[zhi].style.top = resety[zhi] + "%";
				chose[zhi].style.left = resetx[zhi] + "%";
			}
		}else if(isCrash(chose[zhi], kong[2]) && zhi == 5) {
			if(zhi == 5) {
				one(zhi);
			} else {
				chose[zhi].style.top = resety[zhi] + "%";
				chose[zhi].style.left = resetx[zhi] + "%";
			}
		}else if(isCrash(chose[zhi], kong[3]) && zhi == 6) {
			if(zhi == 6) {
				one(zhi);
			} else {
				chose[zhi].style.top = resety[zhi] + "%";
				chose[zhi].style.left = resetx[zhi] + "%";
			}
		}else if(isCrash(chose[zhi], kong[3]) && zhi == 7) {
			if(zhi == 7) {
				one(zhi);
			} else {
				chose[zhi].style.top = resety[zhi] + "%";
				chose[zhi].style.left = resetx[zhi] + "%";
			}
		}else if(isCrash(chose[zhi], kong[4]) && zhi == 8) {
			if(zhi == 8) {
				one(zhi);
			} else {
				chose[zhi].style.top = resety[zhi] + "%";
				chose[zhi].style.left = resetx[zhi] + "%";
			}
		}else if(isCrash(chose[zhi], kong[4]) && zhi == 9) {
			if(zhi == 9) {
				one(zhi);
			} else {
				chose[zhi].style.top = resety[zhi] + "%";
				chose[zhi].style.left = resetx[zhi] + "%";
			}
		}else if(isCrash(chose[zhi], kong[4]) && zhi == 10) {
			if(zhi == 10) {
				one(zhi);
			} else {
				chose[zhi].style.top = resety[zhi] + "%";
				chose[zhi].style.left = resetx[zhi] + "%";
			}
		}else {
			chose[zhi].style.top = resety[zhi] + "%";
			chose[zhi].style.left = resetx[zhi] + "%";
		}

		console.info(num);
		if(num == 11) {
			sucess.get(0).play();
			$(".right").show();
			$(".caidai").show();
			$(".nextnine1").show();
			$(".p22_e1").hide();
			$(".tipp9_1").hide();
			$("#prev22").show();
		}
	}

	function touchstart20(e) {
		var Event = e || window.event;
		var otouch;
		if(Event.touches) {
			otouch = Event.touches[0];
		} else {
			otouch = Event;
		}
		if(num20 >= 4) {
			aud22.load();
		}
		startX = otouch.clientX;
		startY = otouch.clientY;
		zhi20 = this.index;
		// console.log(zhi);
		// console.log(a);
		//阻止默认事件,必须加的
		document.addEventListener(touchMove, touchMove20, false);
		return false;
	}
	function touchMove20(e) {
		var Event1 = e || window.event;
		var otouch;
		var bodyleft = (document.documentElement.clientWidth - content.clientWidth)/2;
		var bodyh = content.clientHeight-20;
		var bodyw = content.clientWidth + bodyleft;
		var bodyh2 = 20;		
		if(Event1.touches) {
			otouch = Event1.touches[0];
			//console.log(1)
		} else {
			otouch = Event1;
			console.log(2);
		}
		endX = otouch.clientX;
		endY = otouch.clientY;
		if(endX<bodyleft||endX>=bodyw||endY<bodyh2||endY>=bodyh){
			document.removeEventListener(touchMove,touchMove20);
			chose20[zhi20].style.top = resety20[zhi20] + "%";
			chose20[zhi20].style.left = resetx20[zhi20] + "%";
		}else{
			chose20[zhi20].style.left = chose20[zhi20].offsetLeft + endX - startX + "px";
			chose20[zhi20].style.top = chose20[zhi20].offsetTop + endY - startY + "px";
		}
		
		startX = endX;
		startY = endY;
		//阻止默认事件,必须加的
		Event1.preventDefault();
		return false;
	}
	function touchend20() {
		document.removeEventListener(touchMove, touchMove20);
		document.removeEventListener(touchStart, touchstart20);
		console.info(zhi20)
		if(isCrash(chose20[zhi20], kong20[4])&&zhi20 == 0) {
			num20++;
			if(zhi20 == 0) {
				size20(zhi20);
				
			} else {
				chose20[zhi20].style.top = resety20[zhi20] + "%";
				chose20[zhi20].style.left = resetx20[zhi20] + "%";
			}
		} else if(isCrash(chose20[zhi20], kong20[3]) && zhi20 == 1) {
			num20++;
			if(zhi20 == 1) {
				size20(zhi20);
				
			} else {
				chose20[zhi20].style.top = resety20[zhi20] + "%";
				chose20[zhi20].style.left = resetx20[zhi20] + "%";
			}
		} else if(isCrash(chose20[zhi20], kong20[2]) && zhi20 == 2) {
			num20++;
			if(zhi20 == 2) {
				size20(zhi20);
			} else {
				chose20[zhi20].style.top = resety20[zhi20] + "%";
				chose20[zhi20].style.left = resetx20[zhi20] + "%";
			}
		} else if(isCrash(chose20[zhi20], kong20[1]) && zhi20 == 3) {
			num20++;
			if(zhi20 == 3) {
				size20(zhi20);
				// $(".p20_text_4").hide();
				$(".p2_25").show().css({opacity:1});
			} else {
				chose20[zhi20].style.top = resety20[zhi20] + "%";
				chose20[zhi20].style.left = resetx20[zhi20] + "%";
			}
		}else if(isCrash(chose20[zhi20], kong20[0]) && zhi20 == 4) {
			num20++;
			if(zhi20 == 4) {
				size20(zhi20);
				// $(".p20_text_5").hide();
				$(".p2_26").show().css({opacity:1});
			} else {
				chose20[zhi20].style.top = resety20[zhi20] + "%";
				chose20[zhi20].style.left = resetx20[zhi20] + "%";
			}
		}else {
			chose20[zhi20].style.top = resety20[zhi20] + "%";
			chose20[zhi20].style.left = resetx20[zhi20] + "%";
		}

		if(num20 == 5) {
			$("#prev24").hide();
			$(".p20_text_6").hide();
			stopaudio();
			sucess.get(0).play();
			setTimeout(function(){
				$(".p4_jiangjie").show();
				$(".p4_jiangjie1").hide();
			},1000);
			aud22.get(0).play();
			aud22.bind("ended",function(){
				$("#prev21").show();
				$(".page20 .p20_go").show();
				$(".p4_jiangjie").hide();
				$(".p4_jiangjie1").show();
			});

			$(".page19 .caidai2").show();
			$("#page19 .p2_2").addClass("p2_2_1");
		}
	}
	//拖拽16事件
	function touchstart16(e) {
		var Event = e || window.event;
		var otouch;
		if(Event.touches) {
			otouch = Event.touches[0];
		} else {
			otouch = Event;
		}
		if(num16 >= 5) {
			$(".ring").load();
		}
		startX = otouch.clientX;
		startY = otouch.clientY;
		zhi16 = this.index;

		//阻止默认事件,必须加的
		document.addEventListener(touchMove, touchMove16, false);
		return false;
	}
	function touchMove16(e) {
		var Event1 = e || window.event;
		var otouch;
		var bodyleft = (document.documentElement.clientWidth - content.clientWidth)/2;
		var bodyh = content.clientHeight-20;
		var bodyw = content.clientWidth + bodyleft;
		var bodyh2 = 20;		
		if(Event1.touches) {
			otouch = Event1.touches[0];
			//console.log(1)
		} else {
			otouch = Event1;
			console.log(2);
		}
		endX = otouch.clientX;
		endY = otouch.clientY;
		if(endX<bodyleft||endX>=bodyw||endY<bodyh2||endY>=bodyh){
			document.removeEventListener(touchMove,touchMove16);
			chose16[zhi16].style.top = resety16[zhi16] + "%";
			chose16[zhi16].style.left = resetx16[zhi16] + "%";
		}else{
			chose16[zhi16].style.left = chose16[zhi16].offsetLeft + endX - startX + "px";
			chose16[zhi16].style.top = chose16[zhi16].offsetTop + endY - startY + "px";
		}
		
		startX = endX;
		startY = endY;
		//阻止默认事件,必须加的
		Event1.preventDefault();
		return false;
	}
	function touchend16() {
		// console.log(num);
		document.removeEventListener(touchMove, touchMove16);
		document.removeEventListener(touchStart, touchstart16);

		if(isCrash(chose16[zhi16], kong16[0])&&zhi16 == 0) {
			if(zhi16 == 0) {
				size16(zhi16);
			} else {
				chose16[zhi16].style.top = resety16[zhi16] + "%";
				chose16[zhi16].style.left = resetx16[zhi16] + "%";
			}
		} else if(isCrash(chose16[zhi16], kong16[1]) && zhi16 == 1) {
			if(zhi16 == 1) {
				size16(zhi16);
			} else {
				chose16[zhi16].style.top = resety16[zhi16] + "%";
				chose16[zhi16].style.left = resetx16[zhi16] + "%";
			}
		} else if(isCrash(chose16[zhi16], kong16[0]) && zhi16 == 2) {
			if(zhi16 == 2) {
				size16(zhi16);
			} else {
				chose16[zhi16].style.top = resety16[zhi16] + "%";
				chose16[zhi16].style.left = resetx16[zhi16] + "%";
			}
		} else if(isCrash(chose16[zhi16], kong16[1]) && zhi16 == 3) {
			if(zhi16 == 3) {
				size16(zhi16);
			} else {
				chose16[zhi16].style.top = resety16[zhi16] + "%";
				chose16[zhi16].style.left = resetx16[zhi16] + "%";
			}
		}else if(isCrash(chose16[zhi16], kong16[0]) && zhi16 == 4) {
			if(zhi16 == 4) {
				size16(zhi16);
			} else {
				chose16[zhi16].style.top = resety16[zhi16] + "%";
				chose16[zhi16].style.left = resetx16[zhi16] + "%";
			}
		}else if(isCrash(chose16[zhi16], kong16[1]) && zhi16 == 5) {
			if(zhi16 == 5) {
				size16(zhi16);
			} else {
				chose16[zhi16].style.top = resety16[zhi16] + "%";
				chose16[zhi16].style.left = resetx16[zhi16] + "%";
			}
		}else if(isCrash(chose16[zhi16], kong16[1]) && zhi16 == 6) {
			if(zhi16 == 6) {
				size16(zhi16);
			} else {
				chose16[zhi16].style.top = resety16[zhi16] + "%";
				chose16[zhi16].style.left = resetx16[zhi16] + "%";
			}
		}else if(isCrash(chose16[zhi16], kong16[1]) && zhi16 == 7) {
			if(zhi16 == 7) {
				size16(zhi16);
			} else {
				chose16[zhi16].style.top = resety16[zhi16] + "%";
				chose16[zhi16].style.left = resetx16[zhi16] + "%";
			}
		}else {
			chose16[zhi16].style.top = resety16[zhi16] + "%";
			chose16[zhi16].style.left = resetx16[zhi16] + "%";
		}

		console.info(num16);
		if(num16 == 8) {
			$(".p16_1,.p16_2,.p16_3,.p16_4,.p16_5,.p16_6,.p16_7,.p16_8").css({
				"cursor": "default"
			});
			$("#page16 .p16blackMaskBg").show();
			$("#page16 .right2").show();
			$("#page16 .caidai2").show();
			$("#page16 .nextk").show();
			$("#page16 .backp16").show();
			sucess.get(0).play();
		}
	}
	//拖拽15事件
	function touchstart15(e) {
		var Event = e || window.event;
		var otouch;
		if(Event.touches) {
			otouch = Event.touches[0];
		} else {
			otouch = Event;
		}
		if(num15 >= 5) {
			$(".ring").load();
		}
		startX = otouch.clientX;
		startY = otouch.clientY;
		zhi15 = this.index;
		// console.log(zhi);
		// console.log(a);
		//阻止默认事件,必须加的
		document.addEventListener(touchMove, touchMove15, false);
		return false;
	}
	function touchMove15(e) {
		var Event1 = e || window.event;
		var otouch;
		var bodyleft = (document.documentElement.clientWidth - content.clientWidth)/2;
		var bodyh = content.clientHeight-20;
		var bodyw = content.clientWidth + bodyleft;
		var bodyh2 = 20;		
		if(Event1.touches) {
			otouch = Event1.touches[0];
			//console.log(1)
		} else {
			otouch = Event1;
			console.log(2);
		}
		endX = otouch.clientX;
		endY = otouch.clientY;
		if(endX<bodyleft||endX>=bodyw||endY<bodyh2||endY>=bodyh){
			console.info(111);
			document.removeEventListener(touchMove,touchMove15);
			chose15[zhi15].style.top = resety15[zhi15] + "%";
			chose15[zhi15].style.left = resetx15[zhi15] + "%";
		}else{
			chose15[zhi15].style.left = chose15[zhi15].offsetLeft + endX - startX + "px";
			chose15[zhi15].style.top = chose15[zhi15].offsetTop + endY - startY + "px";
		}
		
		startX = endX;
		startY = endY;
		//阻止默认事件,必须加的
		Event1.preventDefault();
		return false;
	}
	function touchend15() {
		document.removeEventListener(touchMove, touchMove15);
		document.removeEventListener(touchStart, touchstart15);
		if(isCrash(chose15[zhi15], kuang15[0])&&zhi15 == 0) {
			if(zhi15 == 0) {
				size15(zhi15);
			} else {
				chose15[zhi15].style.top = resety15[zhi15] + "%";
				chose15[zhi15].style.left = resetx15[zhi15] + "%";
			}
		} else if(isCrash(chose15[zhi15], kuang15[0]) && zhi15 == 1) {
			if(zhi15 == 1) {
				size15(zhi15);
			} else {
				chose15[zhi15].style.top = resety15[zhi15] + "%";
				chose15[zhi15].style.left = resetx15[zhi15] + "%";
			}
		} else if(isCrash(chose15[zhi15], kuang15[0]) && zhi15 == 2) {
			if(zhi15 == 2) {
				size15(zhi15);
			} else {
				chose15[zhi15].style.top = resety15[zhi15] + "%";
				chose15[zhi15].style.left = resetx15[zhi15] + "%";
			}
		} else if(isCrash(chose15[zhi15], kuang15[0]) && zhi15 == 3) {
			if(zhi15 == 3) {
				size15(zhi15);
			} else {
				chose15[zhi15].style.top = resety15[zhi15] + "%";
				chose15[zhi15].style.left = resetx15[zhi15] + "%";
			}
		}else if(isCrash(chose15[zhi15], kuang15[0]) && zhi15 == 4) {
			if(zhi15 == 4) {
				size15(zhi15);
			} else {
				chose15[zhi15].style.top = resety15[zhi15] + "%";
				chose15[zhi15].style.left = resetx15[zhi15] + "%";
			}
		}else {
			chose15[zhi15].style.top = resety15[zhi15] + "%";
			chose15[zhi15].style.left = resetx15[zhi15] + "%";
		}

		if(num15 == 5) {
			$("#page15 .right2").show();
			$("#page15 .caidai2").show();
			$("#page15 .backp15").show();
			$("#page15 .nextk").show();
			sucess.get(0).play();
			$(".p15_1,.p15_2,.p15_3,.p15_4,.p15_5,.p15_6,.p15_7").css({
				"cursor":"default"
			});
		}
	}

	function tz22() {
		num = 0;
		for(var i = 0; i < chose.length; i++) {
			chose[i].index = i;
			chose[i].addEventListener(touchStart, touchstart1, false);
			chose[i].addEventListener(touchEnd, touchend1, false);
		}
	}
	function untz22() {
		num = 0;
		for(var i = 0; i < chose.length; i++) {
			chose[i].index = i;
			chose[i].removeEventListener(touchStart, touchstart1, false);
			// chose[i].removeEventListener(touchEnd, touchend1, false);
		}
	}
	function tz20() {
		num20 = 0;
		for(var i = 0; i < chose20.length; i++) {
			chose20[i].index = i;
			chose20[i].addEventListener(touchStart, touchstart20, false);
			chose20[i].addEventListener(touchEnd, touchend20, false);
		}
	}
	function untz20() {
		for(var i = 0; i < chose20.length; i++) {
			chose20[i].index = i;
			chose20[i].removeEventListener(touchStart, touchstart20, false);
			// chose20[i].removeEventListener(touchEnd, touchend20, false);
		}
	}
	function tz16() {
		num16 = 0;
		for(var i = 0; i < chose16.length; i++) {
			chose16[i].index = i;
			chose16[i].addEventListener(touchStart, touchstart16, false);
			chose16[i].addEventListener(touchEnd, touchend16, false);
		}
	}
	function untz16() {
		num16 = 0;
		for(var i = 0; i < chose16.length; i++) {
			chose16[i].index = i;
			chose16[i].removeEventListener(touchStart, touchstart16, false);
			// chose16[i].removeEventListener(touchEnd, touchend16, false);
		}
	}
	function tz15() {
		num15 = 0;
		for(var i = 0; i < chose15.length; i++) {
			chose15[i].index = i;
			chose15[i].addEventListener(touchStart, touchstart15, false);
			chose15[i].addEventListener(touchEnd, touchend15, false);
		}
	}
	function untz15() {
		num15 = 0;
		for(var i = 0; i < chose15.length; i++) {
			chose15[i].index = i;
			chose15[i].removeEventListener(touchStart, touchstart15, false);
			// chose15[i].removeEventListener(touchEnd, touchend15, false);
		}
	}

	//动画流程控制
	$("#next1").on("click touchStart",function(){
		UIplay.next1();
		$(".p1_2").hide();
		$(".p1_2").attr({"src":""});
		$("#prev2").hide();
	});
	$("#next2").on("click touchStart",function(){
		UIplay.next2();
	});
	$("#next3").on(touchStart,function(){
		$("#page3").attr({data:2});
		$("#prev7").hide();
		UIplay.next3();
	});
	$("#next4").on(touchStart,function(){
		$("#prev9").hide();
		$("#page4").hide();
		$("#page4_1").hide();
		$("#page8").show();
		UIplay.next4();
	});
	$("#next5").on(touchStart,function(){
		$("#prev11").hide();
		$("#page8").hide();
		$("#page10").show();
		UIplay.next5();
	});
	$("#next6").on(touchStart,function(){
		stopaudio();
		$("#prev12").hide();
		UIplay.next6();
	});
	$("#next7").on(touchStart,function(){
		$("#prev14").hide();
		$("#page10").hide();
		$("#page8").hide();
		$("#page13").show();
		UIplay.next7();
	});
	$("#next8").on(touchStart,function(){
		$("#prev15").hide();
		num15 = 0;
		UIplay.next8();
	});
	$("#next9").on(touchStart,function(){
		$("#prev16").hide();
		num16 = 0;
		UIplay.next9();
	});
	$("#next10").on(touchStart,function(){
		$("#prev18").hide();
		$("#page8").hide();
		$("#page10").hide();
		$("#page13").hide();
		$("#page17").show();
		UIplay.next10();
	});
	$("#next11").on(touchStart,function(){
		$("#prev21").hide();
		$("#page17").hide();
		$("#page19").show();
		num20 = 0;
		$("#page19 .mubanTextContent img").hide();
		UIplay.next11();
	});
	$("#next12").on(touchStart,function(){
		$("#prev22").hide();
		$("#page19").hide();
		$("#page20").hide();
		num20 = 0;
		$("#page19 .p2_2").removeClass("p2_2_1");
		$("#page22 img").each(function(){
			$(this).removeClass("bounceInUp");
			$(this).removeClass("fadeInLeft");
		 });
		UIplay.next12();
	});
	$("#next13").on(touchStart,function(){
		UIplay.next13();
	});
	//最后的返回事件
	$(".backp").on(touchStart,function(){
		//$("#page23").hide();
		$("#prev22").show();
		UIplay.next12();
		num = 0;
	});
	$("#prev22").on(touchStart,function(){
		$("#prev21").show();
		$("#prev22").hide();
		$("#page22").hide();
		$("#page20").show();
		$("#page19").show();
	
		num20 = 0;
		$("#page19 .mubanTextContent img").hide();
		UIplay.next11();
		
		// $(".page20 img").each(function(){
		// 	$(this).removeAttr("style");
		// });
		$(".page19 .caidai2").removeAttr("style");
	});
	$("#prev21").on(touchStart,function(){
		$(".page20").hide();
		$("#prev21").hide();
		$("#prev18").show();
		$("#page19 .p2_2").removeClass("p2_2_1");
		$("#page4 .suoContent").show();
		$("#page4 .p2_11").show();
		$("#hand").show();
		$("#hand .hand4").show();
		$(".mubanTextContent img").show();
		stopaudio();
		UIplay.next10_1();
	});
	$("#prev18").on(touchStart,function(){
		$("#prev18").hide();
		$("#prev25").show();
		stopaudio();
		$("#page23").hide();
		$("#page17 .suoContent").show();
		$("#page17 .p2_12").css({opacity:1});
		num16 = 0;
		$("#page17").show();
		$("#page18").hide();
		$(".page20").hide();
		$("#page19").hide();
		$("#page22").hide();
		UIplay.next10();
	});
	$("#prev16").on(touchStart,function(){
		$("#prev16").hide();
		$("#prev15").show();
		stopaudio();
		num15 = 0;
		$("#page16").hide();
		UIplay.next8();
	});
	$("#prev15").on(touchStart,function(){
		stopaudio();
		$("#prev15").hide();
		$("#prev14").show();
		UIplay.loadPage();
		$("#page4 .tiziContent img").hide().css({opacity:0});
		$("#page4 .p2_6,#page4 .p2_10,#page4 .p2_8,#page4 .p2_12").show().css({opacity:1});
		$(".hand3").show();
		$(".hand4,#page4 .p2_11").removeClass("flashnews1");
		$("#page15").hide();
		UIplay.next7_1();
	});
	$("#prev14").on(touchStart,function(){
		$("#prev14").hide();
		stopaudio();
		UIplay.loadPage();
		$("#page4 .tiziContent img").hide().css({opacity:0});
		$("#page4 .p2_6,#page4 .p2_10,#page4 .p2_8,#page4 .p2_12").show().css({opacity:1});
		$(".hand3").show();
		$(".hand4,#page13 .p2_11").removeClass("flashnews1");
		$("#page13 .p2_13").css({opacity:1});
		$("#page13 .suoContent").show();
		$("#page13 .p2_12").css({
			opacity:1
		});
		$("#page14").hide();
		$("#prev26").show();
		UIplay.next7();
	});
	$("#prev12").on(touchStart,function(){
		$("#prev12").hide();
		$("#prev11").show();
		stopaudio();
		UIplay.loadPage();
		$("#page12").hide();
		$(".p2_9").show();
		$(".p2_13").show().css({opacity:1});
		$(".p2_12").show();
		$(".p2_10").show().removeClass("flashnews1");
		$(".hand2").show();
		$(".hand3").removeClass("flashnews1");
		UIplay.next5_1();
	});
	$("#prev11").on(touchStart,function(){
		ns = 0;
		stopaudio();
		UIplay.loadPage();
		$("#prev11").hide();
		$("#page11").hide();
		$("#page13").hide();
		$("#page8").hide();
		$("#page10").show();
		$("#page10 .suoContent").show();
		// $("#page4 .tiziContent img").hide().css({opacity:0});
		// $(".p2_6,.p2_8,.p2_14,.p2_13,.p2_12,.p2_10,.p2_11,.p2_9").show().css({opacity:1});
		$(".hand1").show();
		$("#prev27").show();
		$("#page10 .p2_11").removeClass("flashnews1");
		$(".hand2").removeClass("flashnews1");
		UIplay.next5();
	});
	$("#prev9").on(touchStart,function(){
		stopaudio();
		$("#prev9").hide();
		UIplay.loadPage();
		$("#page9").hide();
		$("#page8 .p2_8").css({opacity:1}).show();
		$("#prev28").show();
		UIplay.next4();
	});
	$("#prev7").on(touchStart,function(){
		$("#prev7").hide();
		$("#prev30").show();
		$("#page7").hide();
		$(".p2_16_b").removeAttr("style").hide();
		stopaudio();
		$("#page9").hide();
		$("#page4").hide();
		$("#page4_1").show();
	    UIplay.next3_1();
	});
	$("#prev30").on(touchStart,function(){
		$("#prev30").hide();
		$(".p2_16_b").hide();
		$("#page4 .p2_1,#page4 .p2_2").removeClass("bounceInDown");
		$("#page4").hide();
		$("#prev23").show();
		$("#page7").hide();
		stopaudio();
		$("#page9").hide();
		UIplay.next3();
	});
	$("#prev23").on(touchStart,function(){
		$("#prev23").hide();
		$("#prev3").show();
		UIplay.next2();
	});
	$("#prev24").on(touchStart,function(){
		$("#prev24").hide();
		stopaudio();
		num16 = 0;
		$("#page18").hide();
		$(".mubanTextContent img").show();
		UIplay.next10_1();
	});
	$("#prev25").on(touchStart,function(){
		$("#prev25").hide();
		num16 = 0;
		$("#prev16").show();
		stopaudio();
		$("#page18").hide();
		$("#page17").hide();
		$("#page13").show();
		UIplay.next9();
	});
	$("#prev26").on(touchStart,function(){
		$("#prev26").hide();
		$("#prev12").show();
		stopaudio();
		$("#page18").hide();
		UIplay.next6();
	});
	$("#prev27").on(touchStart,function(){
		$("#prev27").hide();
		$("#prev9").show();
		stopaudio();
		$("#page18").hide();
		$("#page10").hide();
		$("#page8").show();
		$("#page8 .suoContent").show();
		$("#page8 .tiziContent img").removeClass("flashnews1");
		UIplay.next4_2();
	});
	$("#prev28").on(touchStart,function(){
		$("#prev28").hide();
		$("#prev7").show();
		stopaudio();
		$("#page18").hide();
		UIplay.next4_1();
	});
	$("#prev2").on(touchStart,function(){
		$("#page2").hide();
		$("#page1").show();
		//$(".p1_2").attr({"src":""});
		UIplay.load();
	});
	$("#prev3").on(touchStart,function(){
		$("#page3").hide();
		$("#prev3").hide();
		$("#prev2").show();
		UIplay.next1();
	});
	$("#prev4").on(touchStart,function(){
		$(".p20_go").hide();
		UIplay.next2();
	});

});
