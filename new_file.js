var modal = function() {
	function n(n) {
		var o = {
			container: null,
			content: null,
			duration: 2e3,
			hasShade: !0,
			removeShade: !0,
			hasClose: !1,
			autoClose: !1,
			needRemove: !1,
			onclose: null,
			noStyle: !1,
			animation: !0,
			open: {
				duration: 400,
				complete: null
			},
			close: {
				duration: 150
			}
		};
		return $.extend(o, n), o.animation || (o.open.duration = o.close.duration = 0), o
	}

	function o(n) {
		$(n).css({
			position: "absolute",
			visibility: "hidden",
			display: "block"
		})
	}

	function e(n) {
		$(n).css({
			position: "",
			visibility: "",
			display: ""
		})
	}

	function t(n, o, e, t) {
		var a = $(n);
		i(a, "animation-duration", e + "ms"), a.on("webkitAnimationEnd animationend", function() {
			$(this).off("webkitAnimationEnd animationend"), t && t()
		}), i(a, "animation-name", o)
	}

	function i(n, o, e) {
		if("string" == typeof o) c.forEach(function(t) {
			$(n).css(t + o, e)
		});
		else
			for(var t in o) c.forEach(function(e) {
				$(n).css(e + t, o[t])
			})
	}
	var a = 0,
		d = 0,
		l = "modal-shade",
		s = {},
		c = ({
			iOS: /iphone|ipad/i.test(navigator.userAgent),
			Android: /android/i.test(navigator.userAgent),
			WeChat: /micromessenger/i.test(navigator.userAgent)
		}, ["-webkit-", "-ms-"]),
		r = function() {
			return "modal-auto" + ++a
		},
		m = function() {
			var n = $('<div id="' + r() + '" class="modal-wrapper" style="display: none;">   <div class="modal-container">       <div class="modal-content"></div>   </div></div>');
			return s.isLoading && n.find(".modal-content").remove(), w(n), n
		},
		u = function() {
			return "modal-auto" + a
		},
		p = function() {
			var n;
			return n = null != s.container ? s.IsWrapped ? $(s.container).closest(".modal-wrapper") : n = $(s.container) : $("#" + u())
		},
		v = function() {
			var n = p(),
				o = n.find(".modal-content");
			return 0 == o.length && (o = n.find(".modal-container")), o
		},
		f = function() {
			var n = $(document.head);
			0 == $("#modal-style").length && n.prepend('<style id="modal-style">.modal-wrapper{top:0; left:0; right:0; bottom:0; position:fixed; margin:auto; z-index:9999; width:100%;box-sizing:border-box; text-align:center; max-height:100%; overflow-y:auto; -webkit-overflow-scrolling:touch;}.modal-container{display:inline-block; position:relative; margin: 0 auto; text-align:initial; vertical-align:middle;}.modal-content{padding:20px;}.modal-close{color:inherit; font-size:30px; position:absolute; top:-30px; right:0; width:30px; height:30px; line-height:30px; cursor:pointer;}.shade{background-color:#000; opacity:0.6; top:-200%; bottom:0; left:-200%; right:0; height:500%; width:500%; position:fixed; z-index:9998;}</style>')
		},
		h = function(n) {
			n.wrap('<div id="' + r() + '" class="modal-wrapper" style="display:none;"><div class="modal-container"><div class="modal-content">').show();
			var o = p();
			w(o), o.parents(":hidden").show().attr("data-sign", "modal-show")
		},
		g = function(n) {
			n.unwrap("<div class='modal-content'>").unwrap("<div class='modal-container'>").unwrap("<div class='modal-wrapper'>"), $("[data-sign='modal-show']").hide()
		},
		b = "modal-need-remove",
		w = function(n) {
			s.needRemove && n.addClass(b)
		},
		y = function() {
			var n;
			if(f(), s.container) {
				var o = $(s.container);
				!o.hasClass("modal-wrapper") && o.find(".modal-wrapper").length < 1 && (h(o), s.IsWrapped = !0), n = p()
			} else n = m(), $(document.body).append(n), v().html(s.content);
			return n.height(A(n, ".modal-container")), n
		},
		x = function() {
			$('<div id="' + l + '" class="shade"></div>').insertBefore(p())
		},
		k = function() {
			f(), $(document.body).append('<div id="' + l + '" class="shade"></div>')
		},
		C = function() {
			return $("#" + l)
		},
		S = function() {
			C().remove()
		},
		A = function(n, t) {
			var i, a, d = $(n);
			return a = t ? d.find(t) : d, o(d), i = a.height(), 1 > i && (i = I(getComputedStyle(a[0]).height) + I(a.css("margin-top")) + I(a.css("margin-bottom")) + I(a.css("padding-top")) + I(a.css("padding-bottom"))), e(d), i
		},
		I = function(n) {
			try {
				return parseInt(/\d+/.exec(n)[0])
			} catch(o) {
				return console.log(o), null
			}
		},
		W = 0,
		z = function(o) {
			if(W = 1, !($(".modal-wrapper:visible").length > 0)) {
				s = n(o);
				var e = y();
				return s.hasShade && x(s), s.animation ? t(e, "bounceIn", 400, function() {
					s.open.complete && s.open.complete(), W = 2
				}) : (s.open.complete && s.open.complete(), W = 2), e.show(), s.autoClose ? s.autoClose && L(o) : s.isLoading || setTimeout(function() {
					C().click(function() {
						modal.close()
					})
				}, 600), e
			}
		},
		E = function(n) {
			function o(n) {
				n.removeShade && S(), e.hide(), $(".modal-need-remove").remove(), n.IsWrapped && g(i), n.onclose && n.onclose(e), W = 0
			}
			if(!(!s.container && !s.content || 2 > W)) {
				"function" == typeof n ? s.onclose = n : s = $.extend(s, n);
				var e, i;
				e = null != s.container ? $(s.container) : p(), s.IsWrapped && (i = e, e = i.parents(".modal-wrapper")), s.animation ? t(e, "bounceOut", 200, function() {
					o(s)
				}) : o(s)
			}
		},
		R = function(n) {
			if(n = "string" == typeof n ? {
					content: n
				} : n, n.container) return void console.log("message鏂规硶涓嶈兘浼犻€抍ontainer锛岃浣跨敤open");
			n.autoClose = "undefined" == typeof n.autoClose ? !0 : n.autoClose;
			z(n);
			s.onclose = function(o) {
				n.onclose && n.onclose(o), o.remove()
			}
		},
		L = function(n) {
			clearTimeout(d), d = setTimeout(function() {
				E(n)
			}, s.duration)
		},
		T = function(n) {
			for(var o = '<div class="modal-loading-wrap"><div class="loading modal-loading-box">', e = 0; 6 > e; e++) o += '<div class="modal-loading-block"><div class="modal-loading-line"></div><div class="modal-loading-line"></div></div>';
			o += "</div></div>";
			var t = {
				content: o,
				autoClose: !1,
				noStyle: !0,
				animation: !1,
				needRemove: !0,
				isLoading: !0
			};
			z($.extend(t, n))
		},
		O = function(n, o) {
			var e = $('<button type="button" class="btn btn-sm">纭畾</button>').click(o),
				t = $("<div id=" + r() + ' class="modal-wrapper ' + b + '"><div class="modal-container modal-lg"><div class="modal-radius"><div class="modal-content"><div class="message">' + n + '</div></div><div class="modal-foot"><button type="button" class="btn btn-sm" onclick="modal.close()">鍙栨秷</button></div></div></div></div>');
			t.find(".modal-foot").prepend(e), $(document.body).append(t);
			var i = {
				container: t,
				needRemove: !0
			};
			z(i)
		};
	return {
		open: z,
		close: E,
		message: R,
		confirm: O,
		defaults: n,
		loading: T,
		addShade: k,
		removeShade: S
	}
}();
$(function() {
	$.fn.modal = function(n) {
		n || (n = {}), n.container = this, modal.open(n)
	}, $("[data-modal]").click(function() {
		$("#" + $(this).attr("data-modal")).modal()
	}), $("[data-modal-message]").click(function() {
		modal.message($(this).attr("data-modal-message"))
	}), $("[data-dismiss='modal']").click(function() {
		modal.close()
	})
});