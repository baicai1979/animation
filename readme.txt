1.2版本

1 调整上一页功能按钮(需测试)
2 调整小鸭子声音控制按钮点击功能(主界面4)
3 调整拖拽效果(倒数第二个拖拽效果)
4 所有特效已经用手机测试过，因无苹果手机，暂时未测试，在chrome浏览器上模拟测试通过
5 少一个音频与两个握手的图片

1.3版本

1 修改loading效果
2 修改人物位置效果 

1.4版本
1 修改19屏拖拽效果
2 修改第四场景返回上一页的初始时间

1.5版本
1 去除其他版面返回上一页功能，保留最后一页，返回首页功能

1.8版本
1 添加上一页功能
2 修改动画效果、
3 增加初始化功能

1.9版本
1 增加上一页不消失功能
2 调整锦囊弹出的效果
3 调整弹出对话框大小

2.0版本
1 调整锦囊点击次数
2 调整上一页与下一页出现方式

2.1版本
1 调整锦囊点击效果
2 调整梯子消失bug

2.2版本
1 修改音频控制流程
2 修正ios的audio不能自动方法
3 增加按钮控制音频的播放

2.3版本
1 修正touchend的触发事件

2.4版本
1 根据修改意见进行修改
2 增加上一页功能，每个页都增加

3.1版本
1 修改华为手机虚拟键设置
2 修改版面初始化功能
3 修改新出现的几个bug问题